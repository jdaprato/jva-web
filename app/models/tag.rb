class Tag < ApplicationRecord
  has_many :tasks

  validates :name, :presence => true
  validates :color, :presence => true
end

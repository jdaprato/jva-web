class Task < ApplicationRecord
  belongs_to :priority
  belongs_to :status
  belongs_to :user
  belongs_to :tag, :optional => true
  belongs_to :sub_objective, :optional => true

  amoeba do
    enable
  end

  has_many :sub_tasks, dependent: :delete_all

  validates :name, :presence => true
  enum task_category: { is_professional: 'pro', is_personal: 'perso' }
end

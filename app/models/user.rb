class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # Relationships #
  has_many :tasks
  has_many :objectives
  has_many :projects
  has_many :quotations
  has_many :invoices
  has_many :incomes
  has_many :expenses
  has_many :expenses
  has_many :companies
  has_many :contacts


end

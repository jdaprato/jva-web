class Course < ApplicationRecord
  has_many :projects
  has_many :students
end

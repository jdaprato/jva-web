class SubObjective < ApplicationRecord
  # Relationships
  belongs_to :objective
  has_many :tasks
  has_one :achievement
  belongs_to :status
  has_one :recurrent_object
  accepts_nested_attributes_for :recurrent_object, allow_destroy: true, reject_if: :all_blank
  enum sub_objective_category: [:amount, :quantity, :percentage]
  # Validations
  validates :objective, presence: true
  validates :name, :presence => true, uniqueness: true
  validates :description, :presence => true, :allow_blank => true
  validates :sub_objective_category, :presence => true
  validates :value, :presence => true
  validates :status, :presence => true

end

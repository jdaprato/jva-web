class Company < ApplicationRecord
  # Relationships
  has_many :contacts
  has_many :projects
  has_many :quotations
  has_many :invoices
  belongs_to :user

  # Validations
  validates :name, :presence => true, uniqueness: true
  validates :description, :presence => true, :allow_blank => true
  validates :complement_address, :presence => true, :allow_blank => true
  validates :street_address, :presence => true, :allow_blank => true
  validates :zipcode_address, :presence => true, :allow_blank => true
  validates :city_address, :presence => true, :allow_blank => true
  validates :email, :presence => true, :allow_blank => true
  validates :phone, :presence => true, :allow_blank => true

end

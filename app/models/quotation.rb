class Quotation < ApplicationRecord
  # Relationships
  belongs_to :company
  belongs_to :user
  belongs_to :project, optional: true
  has_one :invoice

  has_many :presta_designations, inverse_of: :quotation
  accepts_nested_attributes_for :presta_designations, allow_destroy: true, reject_if: :all_blank
end

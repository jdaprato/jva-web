class Invoice < ApplicationRecord
  belongs_to :quotation
  belongs_to :company
  belongs_to :user
  belongs_to :income, optional: true
  enum status: {:is_ongoing=>0, :is_paid=>1, :is_canceled=>2, :is_archived=>3}
end

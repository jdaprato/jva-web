class Student < ApplicationRecord
  belongs_to :course, optional: true
  belongs_to :project, optional: true
  has_many :oc_sessions
end

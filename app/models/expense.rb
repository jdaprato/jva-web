class Expense < ApplicationRecord
  has_one :recurrent_object, inverse_of: :expense
  belongs_to :user
  accepts_nested_attributes_for :recurrent_object, allow_destroy: true, reject_if: :all_blank
  scope :without_recurrence, -> { left_joins(:recurrent_object).where("expense_id is not null")}
  scope :with_recurrences, -> { left_joins(:recurrent_object).where("expense_id is null") }
end

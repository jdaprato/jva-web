class Project < ApplicationRecord
  # Relationships
  belongs_to :company
  has_many :quotations
  has_many :expertises
  belongs_to :user
  has_many :students
  belongs_to :course, optional: true
end

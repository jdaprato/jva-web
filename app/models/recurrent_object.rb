class RecurrentObject < ApplicationRecord
  belongs_to :expense, optional: true
  belongs_to :income, optional: true

end

class Income < ApplicationRecord
  # Relationships
  has_one :recurrent_object, inverse_of: :income
  belongs_to :invoice, optional: true
  belongs_to :user
  accepts_nested_attributes_for :recurrent_object, allow_destroy: true, reject_if: :all_blank
  # Enum

end
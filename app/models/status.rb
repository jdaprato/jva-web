class Status < ApplicationRecord
  # Relationships
  has_many :tasks
  has_many :sub_objectives

end

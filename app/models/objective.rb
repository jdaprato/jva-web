class Objective < ApplicationRecord
   # Relationships
   has_many :sub_objectives
   belongs_to :user
   # Enum
   enum status: [:is_draft, :is_ongoing, :is_paused, :is_finished, :is_archived ]
   # Validations
   validates :name, :presence => true, uniqueness: true
   validates :description, :presence => true, :allow_blank => true
   validates :start_date, :presence => true
   validates :end_date, :presence => true, :allow_blank => true
   validates :status, :presence => true

end

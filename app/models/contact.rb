class Contact < ApplicationRecord
  # Relationships
  belongs_to :company
  belongs_to :user

  # Validations
  validates :lastname, :presence => true, uniqueness: { scope: [:firstname]}
  validates :firstname, :presence => true, uniqueness: { scope: [:lastname]}
  validates :email, :presence => true, :allow_blank => true
  validates :phone, :presence => true, :allow_blank => true
  validates :is_active, :presence => true, :allow_blank => true
end

class ContactMailer < ApplicationMailer
  default :from => "noreply@jva-web.fr"
  default :to => "jvignauxpro@gmail.com"

  def contact message
      @message = message.message
      @subject = message.subject
      @mail = message.contact_mail
      mail
  end
end

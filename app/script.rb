#!/usr/bin/env ruby
require 'fileutils'
model = ARGV[0] #singular version
controller = ARGV[1]# plural version

puts "******************************* Initializing configuration for model #{model}... *******************************"

# Step 1 : after scaffolding, move the controller file into the backoffice folder #
FileUtils.mv("controllers/#{controller}_controller.rb", "controllers/backoffice/#{controller}_controller.rb")
puts "Moving controller to backoffice folder : OK"

# #Step 2 : move the views files into the backoffice folder #
FileUtils.mv("views/#{controller}", "views/backoffice/#{controller}")
puts "Moving views to backoffice views folder : OK"

# # Step 3 : update the controller #
puts "Updating backoffice controller ..."
lines = File.readlines("controllers/backoffice/#{controller}_controller.rb")
lines[0] = "class Backoffice::#{controller.capitalize}Controller < Backoffice::BackofficeController" << $/
puts "Renaming Backoffice Controller : OK"

lines[2] = "  before_action :get_collections, only: %i[ edit update new create ]" << $/
puts "Adding before_action :get_collections : OK"

lines[68] = "\n  def get_collections\n    @collections  = []\n  end\nend" << $/
puts "Adding get_collections function : OK"
File.open("controllers/backoffice/#{controller}_controller.rb", 'w') {|f|f.write(lines.join)}

# # Step 3.1 : Rename routes inside controller :
# # e.g. : replace @courses by backoffice_courses_path and replace courses_url by backoffice_courses_url
file_content = File.read("controllers/backoffice/#{controller}_controller.rb")
                   .gsub("redirect_to @#{model}", "redirect_to backoffice_#{controller}_path")
                   .gsub("#{controller}_url", "backoffice_#{controller}_url")
File.open("controllers/backoffice/#{controller}_controller.rb", 'w') {|f|f << file_content}
puts "Updating routes into controller : OK"
#
# # Step 4: move routes to backoffice namespace
puts "Updating route file ..."
route_file_lines = File.readlines("../config/routes.rb")
route_file_lines = route_file_lines.insert(10, "    resources :#{controller}\n")
File.open("../config/routes.rb", 'w') {|f|f.write(route_file_lines.join)}
puts "Adding new ressource #{model} into route: OK"
#
# # Step 5 : rewrite completely the file backoffice/courses/new.html.erb
custom_new_form =
  "<h1><%=t('new_backoffice_#{model}') %></h1>
<%= render 'backoffice/shared/custom_form',
          model: @#{model},
          object_name: '#{model}',
          model_params:@attributes,
          collection: @collection || [],
          form_path: backoffice_#{controller}_path,
          action: 'create',
          method: 'post'
%>"
custom_edit_form =
  "<h1><%=t('edit_backoffice_#{model}') %></h1>
<%= render 'backoffice/shared/custom_form',
          model: @#{model},
          object_name: '#{model}',
          model_params:@attributes,
          collection: @collection || [],
          form_path: backoffice_#{controller}_path,
          action: 'update',
          method: 'patch'
%>"
puts "Updating views with custom form ..."
File.open("views/backoffice/#{controller}/new.html.erb", "w"){ |file| file.truncate(0)} # remove all content
File.open("views/backoffice/#{controller}/new.html.erb", "w"){ |file| file.write(custom_new_form)} # remove all content
puts "Creating new form : OK"
File.open("views/backoffice/#{controller}/edit.html.erb", "w"){ |file| file.truncate(0)}
File.open("views/backoffice/#{controller}/edit.html.erb", "w"){ |file| file.write(custom_edit_form)}
puts "Creating edit form : OK"

index_path = File.read("views/backoffice/#{controller}/index.html.erb")
                 .gsub("new_#{model}_path", "new_backoffice_#{model}_path")
                 .gsub("link_to 'Show', #{model}", "link_to 'Show', backoffice_#{model}_path(#{model})")
                 .gsub("link_to 'Edit', edit_student_path(#{model})", "link_to 'Edit', edit_backoffice_#{model}_path(#{model})")
                 .gsub("link_to 'Destroy', #{model}", "link_to 'Destroy', backoffice_#{model}_path(#{model})")
File.open("views/backoffice/#{controller}/index.html.erb", 'w') {|f|f << index_path}


puts "Updating index view paths : OK"
puts "******************************* Your new resource #{model} has been configured successfully ! *******************************"
puts "Next steps:
- Please consider removing unused resource in your config/routes.rb file
- Run rake db:migrate
... and you're ready to go !"

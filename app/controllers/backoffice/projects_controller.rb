class Backoffice::ProjectsController < Backoffice::BackofficeController
  before_action :set_project, only: %i[ show edit update destroy ]
  before_action :get_collections
  # GET /backoffice/projects or /backoffice/projects.json
  def index
    @projects = Project.where(user_id: current_user)
    @grid = ProjectsGrid.new(params.permit![:projects_grid])
    respond_to do |f|
      f.html do
        #@assets = @grid.assets.paginate(page: params[:page], per_page: 20)
      end
      f.csv do
        send_data @grid.to_csv.encode("Windows-1252", :undef => :replace, :replace => ''),
                  type: "text/csv; charset=Windows-1252, header=present",
                  disposition: 'inline',
                  filename: "projects_grid-#{Time.now.to_s}.csv"
      end
      end
  end

  # GET /backoffice/projects/1 or /backoffice/projects/1.json
  def show
  end

  # GET /backoffice/projects/new
  def new
    @project = Project.new

  end

  # GET /backoffice/projects/1/edit
  def edit
  end

  # POST /backoffice/projects or /backoffice/projects.json
  def create
    @project = Project.new(project_params)
    @project.status = "is_ongoing"
    @project.user_id = current_user.id
    respond_to do |format|
      if @project.save
        format.html { redirect_to backoffice_projects_path, notice: "Project was successfully created." }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /backoffice/projects/1 or /backoffice/projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to backoffice_projects_path, notice: "Project was successfully updated." }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /backoffice/projects/1 or /backoffice/projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_projects_url, notice: "Project was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def project_params
      params.require(:project).permit(:name, :description, :slug, :start_date, :end_date, :status, :is_active, :company_id, :user_id, :course_id, :expertise_id)
    end

  def get_collections
    @collections = [Company.all, Course.all, Expertise.all]
  end

end

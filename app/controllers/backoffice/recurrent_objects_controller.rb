class Backoffice::RecurrentObjectsController < Backoffice::BackofficeController
  before_action :set_recurrent_object, only: %i[ show edit update destroy ]

  # GET /recurrent_objects or /recurrent_objects.json
  def index
    @recurrent_objects = RecurrentObject.all
  end

  # GET /recurrent_objects/1 or /recurrent_objects/1.json
  def show
  end

  # GET /recurrent_objects/new
  def new
    @recurrent_object = RecurrentObject.new
  end

  # GET /recurrent_objects/1/edit
  def edit
  end

  # POST /recurrent_objects or /recurrent_objects.json
  def create
    @recurrent_object = RecurrentObject.new(recurrent_object_params)

    respond_to do |format|
      if @recurrent_object.save
        format.html { redirect_to @recurrent_object, notice: "Recurrent object was successfully created." }
        format.json { render :show, status: :created, location: @recurrent_object }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @recurrent_object.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recurrent_objects/1 or /recurrent_objects/1.json
  def update
    respond_to do |format|
      if @recurrent_object.update(recurrent_object_params)
        format.html { redirect_to @recurrent_object, notice: "Recurrent object was successfully updated." }
        format.json { render :show, status: :ok, location: @recurrent_object }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @recurrent_object.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recurrent_objects/1 or /recurrent_objects/1.json
  def destroy
    @recurrent_object.destroy
    respond_to do |format|
      format.html { redirect_to recurrent_objects_url, notice: "Recurrent object was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recurrent_object
      @recurrent_object = RecurrentObject.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def recurrent_object_params
      params.require(:recurrent_object).permit(:periodicity, :start_date, :end_date, :is_active, :comment)
    end
end

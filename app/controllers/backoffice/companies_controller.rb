class Backoffice::CompaniesController < Backoffice::BackofficeController
  before_action :set_company, only: %i[ show edit update destroy ]
  before_action :get_collections, only: %i[ create new edit ]

  # GET /backoffice/companies or /backoffice/companies.json
  def index
    @companies = Company.where(user_id: current_user)
  end

  # GET /backoffice/companies/1 or /backoffice/companies/1.json
  def show
  end

  # GET /backoffice/companies/new
  def new
    @company = Company.new
  end

  # GET /backoffice/companies/1/edit
  def edit
  end

  # POST /backoffice/companies or /backoffice/companies.json
  def create
    @company = Company.new(company_params)
    @company.city_address = company_params[:city_address].upcase
    @company.user_id = current_user.id
    respond_to do |format|
      if @company.save
        format.html { redirect_to backoffice_company_path(@company), notice: "Company was successfully created." }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /backoffice/companies/1 or /backoffice/companies/1.json
  def update
    @company.city_address = company_params[:city_address].upcase
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to backoffice_company_path(@company), notice: "Company was successfully updated." }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /backoffice/companies/1 or /backoffice/companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_companies_url, notice: "Company was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def company_params
      params.require(:company).permit(:name, :description, :complement_address, :street_address, :zipcode_address, :city_address,:email, :phone, :is_active, :user_id)
    end

  def get_collections
    @collections = []
  end
end

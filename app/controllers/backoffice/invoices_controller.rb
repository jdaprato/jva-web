class Backoffice::InvoicesController < Backoffice::BackofficeController
  before_action :set_invoice, only: %i[ show edit update destroy ]

  # GET /backoffice/invoices or /backoffice/invoices.json
  def index
    @invoices = Invoice.where(user_id: current_user)
    @incomes = Income.where(invoice_id: nil, user_id: current_user)
  end

  # GET /backoffice/invoices/1 or /backoffice/invoices/1.json
  def show
  end

  # GET /backoffice/invoices/new
  def new
    @collections = [Quotation.all, Company.all]
    if !params[:quotation].nil?
      @quotation = Quotation.find(params[:quotation])
      @invoice = Invoice.new(
        name: @quotation.name ,
        digit: Time.now.strftime('%Y%m%d'),
        delivery_date: Time.now,
        payment_deadline: Time.now+30.days,
        TVA: @quotation.tva,
        status: 'is_ongoing',
        company_id: @quotation.company_id,
        quotation_id: @quotation.id
      )
    else
      @invoice = Invoice.new(
        digit: Time.now.strftime('%Y%m%d'),
        delivery_date: Time.now,
        payment_deadline: Time.now+30.days,
        status: 'is_ongoing',
      )
    end

  end

  # GET /backoffice/invoices/1/edit
  def edit
    @invoice = Invoice.find(params[:id])
    @collections = [Quotation.all, Company.all]
  end

  # POST /backoffice/invoices or /backoffice/invoices.json
  def create
    @collections = [Quotation.all, Company.all]
    @invoice = Invoice.new(invoice_params)
    @invoice.status = "is_ongoing"
    @invoice.user_id = current_user.id
    respond_to do |format|
      if @invoice.save
        format.html { redirect_to backoffice_invoice_path(@invoice), notice: "Invoice was successfully created." }
        format.json { render :show, status: :created, location: @invoice }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /backoffice/invoices/1 or /backoffice/invoices/1.json
  def update
    # params = invoice_params
    # case invoice_params[:status]
    # when "1"
    #   params[:status] = "is_paid"
    # when "2"
    #   params[:status]= "is_canceled"
    # when "3"
    #   params[:status] = "is_archived"
    # end
    respond_to do |format|
      if @invoice.update(invoice_params)
        format.html { redirect_to backoffice_invoice_path(@invoice), notice: "Invoice was successfully updated." }
        format.json { render :show, status: :ok, location: @invoice }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /backoffice/invoices/1 or /backoffice/invoices/1.json
  def destroy
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_invoices_url, notice: "Invoice was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def invoice_from_quotation
    redirect_to new_backoffice_invoice_path(params[:id])
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def invoice_params
      params.require(:invoice).permit(:name, :digit, :payment_deadline, :delivery_date, :TVA, :deposit, :status, :quotation_id, :company_id, :id_pdfmonkey, :income_ids, :user_id)
    end
end

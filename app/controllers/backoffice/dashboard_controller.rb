class Backoffice::DashboardController < Backoffice::BackofficeController
  include Backoffice::DashboardHelper, Backoffice::GoogleApiHelper, Backoffice::ExpensesHelper

  def index
    # if cookies[:code].nil?
    #   authenticate
    # end
    # Month
    @ongoing_invoices = Invoice.where(status: "is_ongoing")
    @futur_incomes = Array.new
    @ongoing_invoices.each do |invoice|
      @futur_incomes.push(invoice.quotation.total_ttc)
    end
    @monthly_sales = Income.where('credit_date BETWEEN ? and ?', Date.today.beginning_of_month, Date.today).where(income_type: "ca").sum(:amount)
    @monthly_incomes = Income.where('credit_date BETWEEN ? and ?', Date.today.beginning_of_month, Date.today).where(income_type: "other").sum(:amount)
    @monthly_total_charges = calculation_urssaf_cotisations(@monthly_sales) + calculation_cfp(@monthly_sales) + calculation_income_tax(@monthly_sales)

    # Year
    @annual_sales = Income.where(user_id: current_user).where(income_type: "ca").sum(:amount)
    @annual_incomes = Income.where(user_id: current_user).where(income_type: "other").sum(:amount)
    @futur_incomes = @futur_incomes.sum

    CFE_calculation(@annual_sales)
    @total_annual_dues = Expense.where(user_id:current_user.id).where(expense_type: "charge").sum(:amount)
    @total_other_expenses = Expense.where(user_id:current_user.id).where(expense_type: "expense").where('debit_date BETWEEN ? AND ?', Date.today.beginning_of_year, Date.today.end_of_year ).sum(:amount)
    @month_expenses = get_expenses_for_current_month.sum

    # if !cookies[:last_check_sessions].nil? && cookies[:last_check_sessions].to_date < Date.today
    #   get_sessions_for(Date.today.beginning_of_month, Date.today+1.day)
    # end
    @done_sessions_current_month = OcSession.where('session_date BETWEEN ? AND ?', Date.today.beginning_of_month, Date.today.end_of_month)
    #@week_events = get_current_week_events_from_calendar


    # Objectives #rail
    @objectives = Objective.all
  end
  # le ca est considéré comme annuel

  def get_declared_amount
    respond_to do |format|
        format.json { render json: Income.where('credit_date BETWEEN ? and ?', Date.today.beginning_of_month, Date.today).where(income_type: "ca").sum(:amount), status: :ok, cached: true}
    end
  end


end

class Backoffice::SessionsController < Devise::SessionsController
  layout "backoffice", only: [:edit, :update]
  def new
    super
  end

  def create
    super
  end

  def edit
    super
  end

  def update
    super
  end
end
class Backoffice::OcSessionsController < Backoffice::BackofficeController
  before_action :set_oc_session, only: %i[ show edit update destroy ]
  before_action :get_collections, only: %i[ edit update new create ]
  # GET /oc_sessions or /oc_sessions.json
  def index
    @oc_sessions = OcSession.all
  end

  # GET /oc_sessions/1 or /oc_sessions/1.json
  def show
  end

  # GET /oc_sessions/new
  def new
    @oc_session = OcSession.new
  end

  # GET /oc_sessions/1/edit
  def edit
  end

  # POST /oc_sessions or /oc_sessions.json
  def create
    @oc_session = OcSession.new(oc_session_params)

    respond_to do |format|
      if @oc_session.save
        format.html { redirect_to backoffice_oc_sessions_path, notice: "Oc session was successfully created." }
        format.json { render :show, status: :created, location: @oc_session }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @oc_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /oc_sessions/1 or /oc_sessions/1.json
  def update
    respond_to do |format|
      if @oc_session.update(oc_session_params)
        format.html { redirect_to backoffice_oc_sessions_path, notice: "Oc session was successfully updated." }
        format.json { render :show, status: :ok, location: @oc_session }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @oc_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /oc_sessions/1 or /oc_sessions/1.json
  def destroy
    @oc_session.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_oc_sessions_url, notice: "Oc session was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_oc_session
      @oc_session = OcSession.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def oc_session_params
      params.require(:oc_session).permit(:session_date, :session_type, :status, :student_id)
    end

  def get_collections
    @collections  = [Student.all]
  end
end

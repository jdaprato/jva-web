class Backoffice::BackofficeController < ApplicationController
  before_action :authenticate_user!
  before_action :set_attributes_params
  SET_ATTRIBUTES_PARAMS_EXCEPT_CONTROLLERS= ['backoffice/dashboard', 'backoffice/quotation_pdfs', 'backoffice/invoice_pdfs']

  before_action :get_tasks_list
  before_action :get_total_incomes_for_current_month
  before_action :get_total_other_incomes_for_current_month
  #before_action :get_current_week_events_from_calendar
  layout 'backoffice'

  def current_controller
    Rails.application.routes.recognize_path(request.env['PATH_INFO'])[:controller]
  end

  def set_attributes_params
    unless SET_ATTRIBUTES_PARAMS_EXCEPT_CONTROLLERS.include?(params[:controller])
      @attributes = Hash.new
      self.class.name.sub("Controller", "").sub("Backoffice::", "").singularize.constantize.columns_hash.each {|key,value| @attributes[key] = value.type.to_s unless key == "id" || key == "created_at" || key == "updated_at" || key == "is_active" || key =="status" || key == "id_pdfmonkey" || key == "total_ttc" || key == "user_id" || key == "parent_expense"}
    end
  end

  def get_tasks_list
    @pro_tasks_list = Task.where(status_id: 3, task_category: "pro", user_id: current_user).order('priority_id desc') || []
    @perso_tasks_list = Task.where(status_id: 3, task_category:"perso", user_id: current_user).order('priority_id desc') || []
  end

  def get_total_incomes_for_current_month
    @total_incomes = Income.where("EXTRACT(MONTH FROM credit_date) = ?", Time.new.month).sum(:amount)
  end

  def get_total_other_incomes_for_current_month
    @total_other_incomes = Income.where("EXTRACT(MONTH FROM credit_date) = ?", Time.new.month).where(income_type: "other").sum(:amount)
  end

end
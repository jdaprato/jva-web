class Backoffice::RegistrationsController < Devise::RegistrationsController
  layout "backoffice", only: [:edit, :update]
  def new
    super
  end

  def create
    super
  end

  def edit
    @pro_tasks_list = Task.where(status_id: 3, task_category: "pro", user_id: current_user).order('priority_id desc') || []
    @perso_tasks_list = Task.where(status_id: 3, task_category:"perso", user_id: current_user).order('priority_id desc') || []
    super
  end

  def update
    super
  end
end
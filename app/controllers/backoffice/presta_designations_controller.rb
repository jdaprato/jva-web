class Backoffice::PrestaDesignationsController < Backoffice::BackofficeController
  before_action :set_presta_designation, only: %i[ show edit update destroy ]

  # GET /backoffice/pres_designations or /backoffice/pres_designations.json
  def index
    @presta_designations = PrestaDesignation.all
  end

  # GET /backoffice/pres_designations/1 or /backoffice/pres_designations/1.json
  def show
  end

  # GET /backoffice/pres_designations/new
  def new
    @presta_designations = PrestaDesignation.new
  end

  # GET /backoffice/pres_designations/1/edit
  def edit
  end

  # POST /backoffice/pres_designations or /backoffice/pres_designations.json
  def create
    @presta_designations = PrestaDesignation.new(presta_designation_params)

    respond_to do |format|
      if @presta_designations.save
        format.html { redirect_to @presta_designations, notice: "pres designation was successfully created." }
        format.json { render :show, status: :created, location: @presta_designations }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @presta_designations.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /backoffice/pres_designations/1 or /backoffice/pres_designations/1.json
  def update
    respond_to do |format|
      if @presta_designations.update(presta_designation_params)
        format.html { redirect_to @presta_designations, notice: "pres designation was successfully updated." }
        format.json { render :show, status: :ok, location: @presta_designations }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @presta_designations.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /backoffice/pres_designations/1 or /backoffice/pres_designations/1.json
  def destroy
    @presta_designations.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_presta_designations_url, notice: "pres designation was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_presta_designation
      @presta_designations = PrestaDesignation.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def presta_designation_params
      params.require(:presta_designation).permit(:digit, :designation, :quantity, :unit_price)
    end
end

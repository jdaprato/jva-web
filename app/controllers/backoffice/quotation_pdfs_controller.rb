class Backoffice::QuotationPdfsController < Backoffice::BackofficeController
  ID_MODELE='F1285888-1BA1-4D4F-938C-8F4BFC36B322'
  def show
    @quotation = Quotation.find(params[:id])
    if !@quotation.id_pdfmonkey.nil?
      document = Pdfmonkey::Document.fetch(@quotation.id_pdfmonkey)
    else
      document = generate_doc(@quotation)
    end
    redirect_to document.download_url
  end

  def generate_doc quotation
    lines = quotation.presta_designations.order(:number)
    data = {"quotation" => {}, "customer" => {}, "lines" => []}
    data["quotation"]["name"]= quotation.name
    data["quotation"]["number"]= quotation.number
    data["quotation"]["date"]= quotation.created_at.strftime('%d/%m/%Y')
    data["quotation"]["validity_date"]= quotation.validity_date.strftime('%d/%m/%Y')
    data["quotation"]["amount"]= quotation.total_ttc
    data["quotation"]["tva"]= quotation.TVA ? total_ttc*0.8 : 0
    data["quotation"]["deposit_percentage"]= quotation.deposit_percentage
    data["quotation"]["deposit_amount"]= quotation.deposit_percentage * quotation.amount

    data["customer"]["name"] = quotation.company.name
    data["customer"]["address"] = quotation.company.street_address
    data["customer"]["complementAdress"] = quotation.company.complement_address
    data["customer"]["zipcode"] = quotation.company.zipcode_address
    data["customer"]["city"] = quotation.company.city_address
    lines.each do |line|
      line_builder = Hash.new
      line_builder["number"]= line.number
      line_builder['designation']= line.designation
      line_builder['quantity']= line.quantity
      line_builder['unit_price']= line.unit_price
      line_builder['total_ht']= line.unit_price * line.quantity
      data["lines"] << line_builder
    end
    doc_pdfmonkey = Pdfmonkey::Document.generate!(ID_MODELE, data)

    if doc_pdfmonkey.status == 'success'
      quotation.update(id_pdfmonkey: doc_pdfmonkey.id)
      doc_pdfmonkey
    else
      flash[:alert] = "PDF Monkey a rencontré une erreur"
      redirect_back(fallback_location: backoffice_quotations_path)
    end
  end



end
class Backoffice::ObjectivesController < Backoffice::BackofficeController
  before_action :set_objective, only: %i[ show edit update destroy ]
  before_action :get_collections
  # GET /backoffice/objectives or /backoffice/objectives.json
  def index
    @objectives = Objective.where(user_id: current_user)
  end

  # GET /backoffice/objectives/1 or /backoffice/objectives/1.json
  def show
  end

  # GET /backoffice/objectives/new
  def new
    @objective = Objective.new
  end

  # GET /backoffice/objectives/1/edit
  def edit
  end

  # POST /backoffice/objectives or /backoffice/objectives.json
  def create
    @objective = Objective.new(objective_params)
    @objective.status = "is_ongoing"
    @objective.user_id = current_user.id
    respond_to do |format|
      if @objective.save
        format.html { redirect_to backoffice_objective_path(@objective), notice: "Objective was successfully created." }
        format.json { render :show, status: :created, location: @backoffice_objective }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @backoffice_objective.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /backoffice/objectives/1 or /backoffice/objectives/1.json
  def update
    respond_to do |format|
      if @objective.update(objective_params)
        format.html { redirect_to @backoffice_objective, notice: "Objective was successfully updated." }
        format.json { render :show, status: :ok, location: @backoffice_objective }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @backoffice_objective.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /backoffice/objectives/1 or /backoffice/objectives/1.json
  def destroy
    @objective.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_objectives_url, notice: "Objective was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_objective
      @objective = Objective.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def objective_params
      params.require(:objective).permit(:name, :description, :start_date, :end_date, :status, :user_id)
    end

  def get_collections
    @collections = []
  end
end

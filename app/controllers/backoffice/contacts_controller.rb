class Backoffice::ContactsController < Backoffice::BackofficeController
  before_action :set_contact, only: %i[ show edit update destroy ]
  before_action :get_collections, only: %i[new create edit]

  # GET /backoffice/contacts or /backoffice/contacts.json
  def index
    @contacts = Contact.where(user_id: current_user)
  end

  # GET /backoffice/contacts/1 or /backoffice/contacts/1.json
  def show
  end

  # GET /backoffice/contacts/new
  def new
    @contact = Contact.new
  end

  # GET /backoffice/contacts/1/edit
  def edit
    @contact =  Contact.find(params[:id])
  end

  # POST /backoffice/contacts or /backoffice/contacts.json
  def create
    @contact = Contact.new(contact_params)
    contact_params[:lastname].upcase
    contact_params[:firstname].capitalize
    @contact.user_id = current_user.id
    respond_to do |format|
      if @contact.save
        format.html { redirect_to @backoffice_contacts_path, notice: "Contact was successfully created." }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /backoffice/contacts/1 or /backoffice/contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to backoffice_contacts_url, notice: "Contact was successfully updated." }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /backoffice/contacts/1 or /backoffice/contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_contacts_url, notice: "Contact was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def contact_params
      params.require(:contact).permit(:lastname, :firstname, :email, :phone, :is_active, :company_id, :user_id)
    end

  def get_collections
    @collections = [Company.all]
  end
end

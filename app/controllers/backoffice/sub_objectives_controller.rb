class Backoffice::SubObjectivesController < Backoffice::BackofficeController
  skip_before_action :verify_authenticity_token
  before_action :set_sub_objective, only: %i[ show edit update destroy ]
  before_action :get_collections
  # GET /backoffice/sub_objectives or /backoffice/sub_objectives.json
  def index
    @sub_objectives = Objective.where(user_id: current_user).empty? ? [] : Objective.where(user_id: current_user).sub_objectives
  end

  # GET /backoffice/sub_objectives/1 or /backoffice/sub_objectives/1.json
  def show
  end

  # GET /backoffice/sub_objectives/new
  def new
    @sub_objective = SubObjective.new

  end

  # GET /backoffice/sub_objectives/1/edit
  def edit
  end

  # POST /backoffice/sub_objectives or /backoffice/sub_objectives.json
  def create
    create_tasks = params[:sub_objective][:has_tasks]
    @sub_objective = SubObjective.new(sub_objective_params.except(:has_tasks))
    @sub_objective.status = "is_ongoing"
    respond_to do |format|
      if @sub_objective.save
        if create_tasks
          for i in 1..@sub_objective.value.to_i do
            task = Task.new(
              name: @sub_objective.name,
              status_id: 3,
              priority_id: 1,
              task_category: 'pro',
              user_id: current_user.id,
              sub_objective_id: @sub_objective.id
              )
            task.save!
          end
        end
        format.html { redirect_to backoffice_sub_objective_path(@sub_objective), notice: "Sub objective was successfully created." }
        format.json { render :show, status: :created, location: @sub_objective }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @backoffice_sub_objective.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /backoffice/sub_objectives/1 or /backoffice/sub_objectives/1.json
  def update
    create_tasks = sub_objective_params[:has_tasks]
    respond_to do |format|
      if @sub_objective.update(sub_objective_params.except(:has_tasks))
        if create_tasks
          for i in 1..@sub_objective.value.to_i do
            task = Task.new(
              name: @sub_objective.name,
              status_id: 3,
              priority_id: 1,
              task_category: 'pro',
              user_id: current_user.id,
              sub_objective_id: @sub_objective.id
            )
            task.save!
          end
        end
        format.html { redirect_to backoffice_sub_objective_path(@sub_objective), notice: "Sub objective was successfully updated." }
        format.json { render :show, status: :ok, location: @sub_objective }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @sub_objective.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /backoffice/sub_objectives/1 or /backoffice/sub_objectives/1.json
  def destroy
    @sub_objective.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_sub_objectives_url, notice: "Sub objective was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sub_objective
      @sub_objective = SubObjective.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def sub_objective_params
      params.require(:sub_objective).permit(:name, :description, :sub_objective_category, :value, :objective_id, :has_tasks, :progression, :status_id, recurrent_object_attributes: [:id, :periodicity, :start_date, :end_date])
    end

  def get_collections
    @collections = []
  end
end

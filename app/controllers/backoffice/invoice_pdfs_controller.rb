class Backoffice::InvoicePdfsController < Backoffice::BackofficeController
  ID_MODELE='AADB3D82-BCD7-44E6-9C99-9F50369F8B23'
  def show
    @invoice = Invoice.find(params[:id])
    if @invoice.id_pdfmonkey.present?
      document = Pdfmonkey::Document.fetch(@invoice.id_pdfmonkey)
    else
      document = generate_doc(@invoice)
    end

    if document.download_url.nil?
      flash[:alert] = "Le document n'existe pas"
      redirect_back(fallback_location: backoffice_invoices_path)
    else
      redirect_to document.download_url
    end
  end

  def generate_doc invoice
    quotation = Quotation.find(invoice.quotation_id)
    lines = quotation.presta_designations.order('digit::integer')
    data = {"invoice" => {}, "customer" => {}, "lines" => []}
    data["invoice"]["number"]= invoice.digit
    data["invoice"]["date"]= invoice.delivery_date.strftime('%d/%m/%Y')
    data["invoice"]["deliveryDate"]= invoice.delivery_date.strftime('%d/%m/%Y')
    data["invoice"]["dueDate"]= invoice.payment_deadline.strftime('%d/%m/%Y')
    data["invoice"]["tva"]= invoice.TVA ? invoice.TVA : 0
    data["invoice"]["total_ttc"]= invoice.quotation.total_ttc

    data["customer"]["name"] = invoice.company.name
    data["customer"]["address"] = invoice.company.street_address
    data["customer"]["complementAdress"] = invoice.company.complement_address
    data["customer"]["zipcode"] = invoice.company.zipcode_address
    data["customer"]["city"] = invoice.company.city_address
    lines.each do |line|
      line_builder = Hash.new
      line_builder["number"]= line.digit
      line_builder['designation']= line.designation
      line_builder['quantity']= line.quantity
      line_builder['unit_price']= line.unit_price
      line_builder['total_ht']= line.unit_price * line.quantity
      data["lines"] << line_builder
    end
    doc_pdfmonkey = Pdfmonkey::Document.generate!(ID_MODELE, data)

    if doc_pdfmonkey.status == 'success'
      invoice.update(id_pdfmonkey: doc_pdfmonkey.id)
      doc_pdfmonkey
    else
      flash[:alert] = "PDF Monkey a rencontré une erreur"
      redirect_back(fallback_location: backoffice_invoices_path)
    end
  end

end

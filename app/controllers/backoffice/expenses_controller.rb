class Backoffice::ExpensesController <  Backoffice::BackofficeController
  before_action :set_expense, only: %i[ show edit update destroy ]
  before_action :get_collections, only: %i[ edit update new create ]

  # GET /expenses or /expenses.json
  def index
    @expenses = Expense.where(user_id: current_user.id, parent_expense: nil)
  end

  # GET /expenses/1 or /expenses/1.json
  def show
  end

  # GET /expenses/new
  def new
    @expense = Expense.new
  end

  # GET /expenses/1/edit
  def edit
  end

  # POST /expenses or /expenses.json
  def create
    @expense = Expense.new(expense_params)
    @expense.user_id = current_user.id
    respond_to do |format|
      if @expense.save
        if !@expense.recurrent_object.nil?
          case @expense.recurrent_object.periodicity
          when "Mensuel"
            @beginning_date = @expense.recurrent_object.start_date
            @end_date = !@expense.recurrent_object.end_date.nil? ? @expense.recurrent_object.end_date : @expense.recurrent_object.start_date + 1.year
            @expense_date = []
            @end_date =[@end_date.month, @end_date.year]
            @new_expense = @expense
            while @expense_date != @end_date
              @cloned_expense = clone(@expense, @new_expense)
              @expense_date = [@cloned_expense.debit_date.month, @cloned_expense.debit_date.year]
              @new_expense = @cloned_expense
            end
          end
        end
        format.html { redirect_to backoffice_expenses_path, notice: "Expense was successfully created." }
        format.json { render :show, status: :created, location: @expense }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /expenses/1 or /expenses/1.json
  def update
    respond_to do |format|
      if @expense.update(expense_params)
        format.html { redirect_to backoffice_expenses_path, notice: "Expense was successfully updated." }
        format.json { render :show, status: :ok, location: @expense }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /expenses/1 or /expenses/1.json
  def destroy
    @expense.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_expenses_url, notice: "Expense was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_expense
      @expense = Expense.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def expense_params
      params.require(:expense).permit(:name, :description, :status, :debit_date, :amount, :comment, :expense_type, recurrent_object_attributes: [:id, :periodicity, :start_date, :end_date])
    end
    def get_collections
      @collections  = []
    end

    def clone main_expense, expense
      @cloned_expense = expense.dup
      @cloned_expense.parent_expense = main_expense.id
      @cloned_expense.debit_date = expense.recurrent_object.nil? ? expense.debit_date + 1.month : expense.recurrent_object.start_date + 1.month
      @cloned_expense.save!
      @cloned_expense
    end

end

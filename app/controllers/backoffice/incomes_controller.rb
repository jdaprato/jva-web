class Backoffice::IncomesController < Backoffice::BackofficeController
  before_action :set_income, only: %i[ show edit update destroy ]
  before_action :get_collections, only: %i[ edit update new create ]
  # GET /backoffice/incomes or /backoffice/incomes.json
  def index
    @incomes= Income.where(user_id: current_user.id).order('credit_date DESC')
  end

  # GET /backoffice/incomes/1 or /backoffice/incomes/1.json
  def show
  end

  # GET /backoffice/incomes/new
  def new
    @income = Income.new
  end

  # GET /backoffice/incomes/1/edit
  def edit
  end

  # POST /backoffice/incomes or /backoffice/incomes.json
  def create
    @income = Income.new(income_params)
    @income.user_id = current_user.id
    respond_to do |format|
      if @income.save
        format.html { redirect_to backoffice_incomes_path, notice: "income was successfully created." }
        format.json { render :show, status: :created, location: @incomes}
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @incomes.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /backoffice/incomes/1 or /backoffice/incomes/1.json
  def update
    respond_to do |format|
      if @income.update(income_params)
        format.html { redirect_to backoffice_incomes_path, notice: "income was successfully updated." }
        format.json { render :show, status: :ok, location: @incomes}
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @incomes.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /backoffice/incomes/1 or /backoffice/incomes/1.json
  def destroy
    @income.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_incomes_url, notice: "income was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_income
    @income = Income.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def income_params
    params.require(:income).permit(:name, :amount, :description, :credit_date, :comment, :invoice_id, :income_type, recurrent_object_attributes: [:id, :periodicity, :start_date, :end_date])
  end

  def get_collections
    @collections  = [Invoice.all]
  end
end

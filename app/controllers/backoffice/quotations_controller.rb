class Backoffice::QuotationsController < Backoffice::BackofficeController
  before_action :set_quotation, only: %i[ show edit update destroy ]
  before_action :get_collections, only: %i[ new create edit ]

  # GET /quotations or /quotations.json
  def index
    @quotations = Quotation.where(user_id: current_user)
  end

  # GET /quotations/1 or /quotations/1.json
  def show
  end

  # GET /quotations/new
  def new
    @quotation = Quotation.new(digit: Time.now.strftime('%Y%m%d'), validity_date: Time.now+ 1.month, deposit_percentage: 30, status: 'is_ongoing', total_ttc: 0)
  end

  # GET /quotations/1/edit
  def edit
  end

  # POST /quotations or /quotations.json
  def create
    @quotation = Quotation.new(quotation_params)
    @quotation[:total_ttc] = get_total_ttc
    @quotation[:status] = "is_ongoing"
    @quotation.user_id = current_user.id
    respond_to do |format|
      if @quotation.save
        format.html { redirect_to backoffice_quotation_path(@quotation), notice: "Quotation was successfully created." }
        format.json { render :show, status: :created, location: @quotation }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @quotation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quotations/1 or /quotations/1.json
  def update
    respond_to do |format|
      if @quotation.update(quotation_params)
        @quotation.update(total_ttc: get_total_ttc)
        format.html { redirect_to backoffice_quotation_path(@quotation), notice: "Quotation was successfully updated." }
        format.json { render :show, status: :ok, location: @quotation }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @quotation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quotations/1 or /quotations/1.json
  def destroy
    @quotation.destroy
    respond_to do |format|
      format.html { redirect_to backoffice_quotations_url, notice: "Quotation was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quotation
      @quotation = Quotation.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def quotation_params
      params.require(:quotation).permit(:digit, :name, :validity_date, :total_ttc, :tva, :deposit_percentage, :status, :is_active, :company_id, :project_id, :user_id, presta_designations_attributes: [:id, :digit, :designation, :quantity, :unit_price, :_destroy])
    end

    def get_collections
      @collections  = [Company.all, Project.all]
    end

  def get_total_ttc
    prestations = @quotation.presta_designations
    total_ttc = Array.new
    prestations.each do |presta|
      total_ttc << presta.quantity.to_i * presta.unit_price.to_f
    end
    if @quotation.tva
      total_ttc = total_ttc.sum + (total_ttc.sum * 0.8)
    end
    total_ttc.sum
  end

end

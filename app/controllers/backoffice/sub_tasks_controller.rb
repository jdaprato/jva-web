class Backoffice::SubTasksController < Backoffice::BackofficeController
  before_action :set_sub_task, only: %i[ show edit update destroy ]
  # before_action :get_collections, only: %i[ edit update new create ]
  # GET /sub_tasks or /sub_tasks.json
  def index
    @sub_tasks = SubTask.all
  end

  # GET /sub_tasks/1 or /sub_tasks/1.json
  def show
  end

  # GET /sub_tasks/new
  def new
    @sub_task = SubTask.new
  end

  # GET /sub_tasks/1/edit
  def edit
  end

  # POST /sub_tasks or /sub_tasks.json
  def create
    @sub_task = SubTask.new(sub_task_params)
    respond_to do |format|
      if @sub_task.save
        format.json { render json: @sub_task, status: :created}
      else
        format.json { render json: @sub_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sub_tasks/1 or /sub_tasks/1.json
  def update
    respond_to do |format|
      if @sub_task.update(sub_task_params)
        format.json { render json: @sub_task, status: :ok}
      else
        format.json { render json: @sub_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sub_tasks/1 or /sub_tasks/1.json
  def destroy
    @sub_task.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sub_task
      @sub_task = SubTask.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def sub_task_params
      params.require(:sub_task).permit(:name, :status_id, :task_id)

  # def get_collections
  #   @collections  = []
  # end
end
end

class Backoffice::TasksController < Backoffice::BackofficeController
  #include GoogleCalendar
  before_action :set_task, only: %i[ show edit update destroy ]
  skip_before_action :verify_authenticity_token

  #before_action :get_current_agenda

  # GET /tasks or /tasks.json
  def index
    @tasks = Task.all
  end

  def kanban
    if request.xhr?
      @tasks_planned = Task.where(status: 3).where.not(due_date: nil)
      respond_to do |format|
        format.json {render json: @tasks_planned}
      end
    end
    @tasks = Task.where(status: 3).where(due_date: nil)
    @tasks_planned = Task.where(status: 3).where.not(due_date: nil)
    @days_of_week = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
    @tags = Tag.all
    @tag = Tag.new
    @sub_task = SubTask.new
    @collections = []
  end

  # GET /tasks/1 or /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks or /tasks.json
  def create
    custom_params = task_params
    custom_params['status_id'] = Status.where(name: 'is_ongoing').ids[0]
    custom_params['priority_id'] = Priority.where(name: 'low').ids[0]
    @task = Task.new(custom_params)
    @task.user_id = current_user.id
    respond_to do |format|
      if @task.save
        format.html { redirect_to params[:source_path], notice: "Task was successfully created." }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1 or /tasks/1.json
  def update
    @task = Task.find(params['id'])
    custom_params = JSON.parse(params['backoffice_task'])
    if custom_params['due_date'].nil? && @task.due_date.nil?
      custom_params['due_date'] = nil
    end
    if !params['source_path'].nil? && !params['custom_action'].nil?
      source_path = JSON.parse(params['source_path'])
      action = JSON.parse(params['custom_action'])

      if action == 'update_priority'
        if @task.priority.name == 'low'
          custom_params['priority_id'] = 2
        elsif @task.priority.name == 'medium'
          custom_params['priority_id'] = 3
        else
          custom_params['priority_id'] = 1
        end
      else action =='update_status'
      if @task.status.name == 'is_ongoing'
        custom_params['status_id'] = 4;
      end
      end
    end
    respond_to do |format|
      if @task.update(custom_params)
        format.html { redirect_to source_path.nil? ? backoffice_kanban_path : source_path, notice: "Task was successfully updated." }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1 or /tasks/1.json
  def destroy
    if params['source_path']
      source_path = JSON.parse(params['source_path'])
    end
    @task.destroy
    respond_to do |format|
      format.html { redirect_to source_path ? source_path : backoffice_tasks_path, alert: "Task was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def clone_task
    @new_task = Task.find(params[:task_id]).amoeba_dup
    @new_task.save
    respond_to do |format|
      format.html { redirect_to backoffice_kanban_path, notice: "Task cloned !." }
      format.json { render json: @new_task, status: :created }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_task
    @task = Task.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def task_params
    params.require(:task).permit(:name, :priority_id, :task_category, :status_id, :due_date, :tag_id)
  end
end

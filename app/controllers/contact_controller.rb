class ContactController < ApplicationController
  def new
    @message = Message.new
  end

  def create
    @message = Message.new(params[:message])
    if @message.valid? && verify_recaptcha(model: @message)
      ContactMailer.contact(@message).deliver_now
      flash.now.alert = "Merci le message a bien été envoyé !"
      redirect_to(root_path, :notice => "Le mail a bien été envoyé !")
    else
      flash.now.alert = "Please fill all fields."
      redirect_to(root_path(:anchor => "contact"), :alert => "Please fill all fields.")
    end
  end
end

module ApplicationHelper

  def prepare_collection_for_form collection
    "#{collection.klass}_id".downcase.to_s.gsub('::', '_')
  end

  def link_to_add_fields(name, f, association)
    new_object = @quotation.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields mb-2", data: {id: id, fields: fields.gsub("\n", "")})
  end

end

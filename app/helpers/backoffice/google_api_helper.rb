module Backoffice::GoogleApiHelper

  def authenticate
    begin
      client = Signet::OAuth2::Client.new(client_options)
      redirect_to client.authorization_uri.to_s
    rescue Google::Apis::AuthorizationError
      response = client.refresh!
      session[:authorization] = session[:authorization].merge(response)
      retry
    end
  end


  def callback
    code = params[:code]
    client = Signet::OAuth2::Client.new(client_options)
    client.code = code
    cookies[:code] = code
    response = client.fetch_access_token!
    session[:authorization] = response
    redirect_to backoffice_root_url
  end

  def get_sessions_for(begin_date, end_date)
    begin
      client = Signet::OAuth2::Client.new(client_options)
      client.update!(session[:authorization])

      service = Google::Apis::CalendarV3::CalendarService.new
      service.authorization = client
      @events_list = service.list_events(
        "jvignauxpro@gmail.com",
        single_events: true,
        order_by: "startTime",
        time_min: begin_date.rfc3339,
        time_max:end_date.rfc3339
      )

      @list_sessions = Array.new
      @session = Hash.new
      @events_list.items.each do |event|
        if event.summary.include?("Session")
          if !event.description.nil?
            if JSON(event.description)["type"] == "session"
              @list_sessions.push(@session["oc_session"] = {
                "session_date": event.start.date_time.to_datetime,
                "session_type": JSON(event.description)["type"],
                "student_id": JSON(event.description)["student_id"],
                "status": JSON(event.description)["status"]})
            end
          end
        end
      end
      @list_sessions.each do |session|
        new_session = OcSession.new(session)
        if !OcSession.exists?(session)
          new_session.save!
        end
      end
      cookies[:last_check_sessions] = Date.today
    rescue Google::Apis::AuthorizationError
      authenticate
    end

  end

  def get_current_week_events_from_calendar
      if !cookies[:google_api] || cookies[:code].nil?
        authenticate
        cookies[:google_api] = true
      else
        begin
          client = Signet::OAuth2::Client.new(client_options)
          client.update!(session[:authorization])

          service = Google::Apis::CalendarV3::CalendarService.new
          service.authorization = client

          @events_list = service.list_events(
            "jvignauxpro@gmail.com",
            single_events: true,
            order_by: "startTime",
            time_min: Date.today.beginning_of_week.rfc3339,
            time_max: Date.today.end_of_week.rfc3339
          )
          @week_events = Array.new

          @events_list.items.each do |event|
            @week_events.push(event)
          end
        rescue Google::Apis::AuthorizationError
          response = client.refresh!
          session[:authorization] = session[:authorization].merge(response)
        end
      end
    end

  private

  def client_options
    {
      client_id: ENV['GOOGLE_CLIENT_ID'] || Rails.application.secrets.google_client_id,
      client_secret: ENV['GOOGLE_CLIENT_SECRET'] || Rails.application.secrets.google_client_secret,
      authorization_uri: 'https://accounts.google.com/o/oauth2/auth',
      token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
      scope: Google::Apis::CalendarV3::AUTH_CALENDAR,
      redirect_uri: "http://localhost/backoffice/callback"
    }
  end


end
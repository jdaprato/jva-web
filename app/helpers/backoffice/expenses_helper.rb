module Backoffice::ExpensesHelper

  def get_expenses_for_current_month
    @expenses_for_current_month = []
    @expenses = Expense.where(user_id: current_user.id)
    @expenses.each do |expense|
      if expense.debit_date.nil?
        if !expense.recurrent_object.nil?
          if expense.recurrent_object.start_date.month == Date.today.month
            @expenses_for_current_month.push(expense.amount)
          end
        end
      else
        if expense.debit_date.month == Date.today.month
          @expenses_for_current_month.push(expense.amount)
        end
      end
    end
    @expenses_for_current_month
  end
end

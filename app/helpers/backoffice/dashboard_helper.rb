module Backoffice::DashboardHelper
  COTISATIONS = 0.22
  INCOME_TAX = 0.022
  CFP = 0.002
  SEUIL_MICRO_ENTERPRISE = 72600
  FRANCHISE_TVA = 36500

  def calculation_urssaf_cotisations sales
    sales * COTISATIONS
  end

  def CFE_calculation(sales)
    case sales
    when sales < 5000
      @cfe = 0
    when sales > 5000 && sales < 10000
      @cfe_min = 223
      @cfe_max = 531
    when sales > 10000 && sales <= 32600
      @cfe_min = 223
      @cfe_max = 1061
    when sales > 32600 && sales <= 10000
      @cfe_min = 223
      @cfe_max = 2229
    when sales > 100000 && sales <= 250000
      @cfe_min = 223
      @cfe_max = 3716
    when sales > 250000 && sales <= 500000
      @cfe_min = 223
      @cfe_max = 5307
    when sales > 500000
      @cfe_min = 223
      @cfe_max = 6901
    end
  end

  def calculation_income_tax sales
    sales * INCOME_TAX
  end

  def calculation_cfp sales
    sales * CFP
  end

  def franchise_tva sales
    if sales < FRANCHISE_TVA
      true
    else
      false
    end
  end

  def seuil_micro_entreprise sales
    if sales > SEUIL_MICRO_ENTERPRISE
      true
    else
      false
    end
  end


end

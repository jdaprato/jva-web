import Rails from "@rails/ujs";

/** Delete or update status or priority task **/
function update_task(tasks, method, action){
    for (let i = 0; i < tasks.length; i++) {
        tasks[i].addEventListener('click', function(){
            let task_id = this.closest('div').querySelector('li').id || this.closest('.card').id;
            let form = new FormData(),
                data =  {
                    id: task_id,
                };
            form.append("backoffice_task", JSON.stringify(data));
            form.append("source_path", JSON.stringify(window.location.href));
            form.append("custom_action", JSON.stringify(action));
            Rails.ajax({
                type: method,
                url: '/backoffice/tasks/'+task_id,
                data: form,
                success: function(response){
                    console.log("success: "+response);
                },
                error: function(response){
                    console.log("failure: "+response);
                }
            });
        });
    }
}

document.addEventListener("turbolinks:load", function() {
    let task_priority = document.querySelectorAll('.change_priority'),
        task_validated = document.querySelectorAll('.validate_task'),
        task_deleted = document.querySelectorAll('.delete_task');
    update_task(task_priority, "PATCH", "update_priority");
    update_task(task_validated, "PATCH", "update_status");
    update_task(task_deleted, "DELETE");
});



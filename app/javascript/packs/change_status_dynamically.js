import { Modal } from 'bootstrap'
const url = "/backoffice/invoices/";

function updateInvoiceStatus(invoiceId, data){
    fetch(url+invoiceId, {method: 'PATCH', headers: headers, body: JSON.stringify(data)})
        .then(response => response.text())
        .then(text => {
            eval(text);
        })
}

document.addEventListener("turbolinks:load", function() {
    let select_tags = document.querySelectorAll('.status');

    /* Display a modal with the list of available incomes */
    select_tags.forEach((select_tag)=> {
        select_tag.addEventListener("change", function(e){
            let invoiceId = select_tag.parentNode.parentElement.children[0].id;
            let myModal = new Modal(document.getElementById('add_incomes_modal_'+invoiceId));
            let selectedValue = this.options[this.selectedIndex].value;
            let body = {invoice: {id: invoiceId, status: selectedValue}};

            const token = document.getElementsByName(
                "csrf-token"
            )[0].content;
            let httpHeaders = {'X-CSRF-Token': token}
            const headers = new Headers(httpHeaders);
            headers.append('Content-Type', 'application/json')
            //formData backoffice_invoice :{"id":"41"}
            if(selectedValue == "is_paid"){// a refacto
                myModal.show();
            } else {
                updateInvoiceStatus(invoiceId, body);
            }
        });
    });

    /* Calculation of total selected amounts */
    let checkboxes = document.querySelectorAll('.income_checkbox');

    let total_amount = document.querySelector('#total_amount'),
        sum = 0;
    checkboxes.forEach((checkbox) => {
        checkbox.addEventListener('click', (e) => {
            let input_value = parseFloat(e.target.parentNode.nextElementSibling.dataset.amount);
            if (e.target.checked){
                sum += input_value;
            }
            total_amount.textContent = sum;
        })
    });


});

class addRecurrentTransactionForm {
    constructor() {
        this.switch = document.querySelector('#recurrent_object_switch')
        this.switch_value = document.querySelector('.recurrent_object_switch').checked
        this.form = document.querySelector('.recurrent_object_form')
        this.initializeListener();
    }

    initializeListener() {
        if (this.switch === undefined) return
        this.switch.addEventListener('click', e => {
            if(document.querySelector('.recurrent_object_switch').checked){
                this.form.classList.remove("d-none");
            } else {
                this.form.classList.add("d-none");
            }
        })
    }
}

// Wait for turbolinks to load, otherwise `document.querySelectorAll()` won't work
window.addEventListener('turbolinks:load', () => new addRecurrentTransactionForm())
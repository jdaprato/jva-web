import Glide, { Autoplay, Controls, Swipe } from '@glidejs/glide/dist/glide.modular.esm'
document.addEventListener("turbolinks:load", function() {
    this.Glide =
    new Glide('.glide', {
        autoplay: 7000,
        type: 'slider'
    }).mount({ Autoplay, Controls, Swipe });

    /* Fix error see https://github.com/glidejs/glide/issues/334 */
    const ArrowRight = document.querySelector('.glide__arrow--right');
    if(ArrowRight){
        ArrowRight.addEventListener('click', () => {
            this.Glide.go('>');
        });

    }
    const Arrowleft = document.querySelector('.glide__arrow--left');
    if (Arrowleft){
        Arrowleft.addEventListener('click', () => {
            this.Glide.go('<');
        });
    }
    /* Typewrite animation banner */
    let TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        let i = this.loopNum % this.toRotate.length;
        let fullTxt = this.toRotate[i];

        if (this.isDeleting) {
            this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
            this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<h1 class="wrap">'+this.txt+'</h1>';

        let that = this;
        let delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
            delta = this.period;
            this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
            this.isDeleting = false;
            this.loopNum++;
            delta = 50;
        }

        setTimeout(function() {
            that.tick();
        }, delta);
    };

    window.onload = function() {
        let elements = document.getElementsByClassName('typewrite');
        for (let i=0; i<elements.length; i++) {
            let toRotate = elements[i].getAttribute('data-type');
            let period = elements[i].getAttribute('data-period');
            if (toRotate) {
                new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        let css = document.createElement("style");
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
});

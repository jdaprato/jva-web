import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"

Rails.start()
Turbolinks.start()
ActiveStorage.start()

import "bootstrap"
import "../stylesheets/backoffice.scss"
const images = require.context('../images', true)
import "./tasks"
import "./nested-forms/addFields"
import "./nested-forms/removeFields"
// import "./nested-forms/addRecurrentTransactionForm"
import "./calculate_PE"
import "./change_status_dynamically"
import "./kanban"
import "chartkick/chart.js"
require("chartkick")
require("chart.js")


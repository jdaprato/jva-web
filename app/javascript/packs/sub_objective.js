document.addEventListener("turbolinks:load", () => {
    let sub_objectives_progressions = document.querySelectorAll('.sub_objective_progression');
    sub_objectives_progressions.forEach((progression) => {
       progression.addEventListener(('change'), (e) => {
           let sub_objective_id = e.target.dataset.sub_objective_id;
           const token = document.getElementsByName(
               "csrf-token"
           )[0].content;
           let form = new FormData(),
               data =  {
               sub_objective: {
                       progression: e.target.value,
                       token: token,
                   }
               };

           let httpHeaders = {'X-CSRF-Token': token}
           const headers = new Headers(httpHeaders);
           headers.append('Content-Type', 'application/json')
           form.append("backoffice_sub_objective", JSON.stringify(data));
           fetch('/backoffice/sub_objectives/'+sub_objective_id, {
               method: "PATCH",
               headers: headers,
               body: {
                   form,
               }
           })
               .then(() => console.log("success"))
               .catch((e) => console.log(e));
       });
    });
});
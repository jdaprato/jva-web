import CookiesEuBanner from 'cookies-eu-banner'
import 'cookies-eu-banner/css/cookies-eu-banner.default.css'

document.addEventListener("turbolinks:load", function() {
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        // return "";
    }
    let accept_button = document.querySelector('#cookies-eu-accept'),
        reject_button = document.querySelector('#cookies-eu-reject'),
        cookies_banner = document.querySelector('#cookies-eu-banner'),
        current_date = new Date(),
        in_3_months = new Date(current_date.setMonth(current_date.getMonth()+3)),
        expiration_date = in_3_months.toUTCString();
    if(getCookie("hasConsent") !== undefined){
        cookies_banner.style.display = 'none';
    } else {
        new CookiesEuBanner(() => { // creation of a new cookie "hasConsent" with true value
            /* The cookies are accepted */
            accept_button.addEventListener('click', () => {
                /* Removal of cookies banner */
                cookies_banner.style.display = "none";
            });
            /* The cookies are rejected */
            reject_button.addEventListener('click', ()=> {
                /* Create a cookie for the user with consent to no */
                let cookie = getCookie("hasConsent");
                cookie = "hasConsent=false; expires="+expiration_date;
                /* Removal of cookies banner */
                cookies_banner.style.display = "none";
            });
        })
    }
})
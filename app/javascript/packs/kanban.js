import Rails from '@rails/ujs';
import {clone} from "chart.js/helpers";
document.addEventListener('turbolinks:load', () => {
    /* Draggable events */
    let tasks = document.querySelectorAll('.tasks');
    let drop_zones = document.querySelectorAll('.drop_zone');

    tasks.forEach((task) => {
        task.addEventListener('dragstart', (e) => {
            e.dataTransfer.setData('target_id', e.target.id);
            e.dataTransfer.dropEffect = 'move';
        });
        task.addEventListener('drop', (e) => {
            e.dataTransfer.dropEffect = 'copy';
            let task_id = e.target.closest('.tasks').id;
            let card = e.target.closest('.card-body');
            let tag_id = e.dataTransfer.getData('tag_id');
            let clone = document.getElementById(tag_id).cloneNode(true);
            card.insertBefore(clone, card.children[0]);

            /* Update task with tag */
            let form = new FormData(),
                data =  {
                    id: task_id,
                    tag_id: tag_id ? tag_id : ''
                };
            form.append('backoffice_task', JSON.stringify(data));
            Rails.ajax({
                type: 'PATCH',
                url: '/backoffice/tasks/'+task_id,
                data: form,
                success: () => {
                    //Removal of id to avoid duplicate
                    clone.id = "";
                },
                error: function(response){
                    console.log('failure: '+response);
                }
            });
        });
    });

    drop_zones.forEach((drop_zone) => {
        drop_zone.addEventListener('dragover', (e) => {
            e.preventDefault();
            e.dataTransfer.dropEffect = 'move';
        });
        drop_zone.addEventListener('drop', (e) => {
            let task_id = e.dataTransfer.getData('target_id');
            e.target.appendChild(document.getElementById(task_id));
            let due_date = e.target.firstChild.nextSibling.dataset.due_date;
            let form = new FormData(),
                data =  {
                    id: task_id,
                    due_date: due_date ? due_date : ''
                };
            form.append('backoffice_task', JSON.stringify(data));
            Rails.ajax({
                type: 'PATCH',
                url: '/backoffice/tasks/'+task_id,
                data: form,
                success: function(response){
                    console.log('success: '+response);
                },
                error: function(response){
                    console.log('failure: '+response);
                }
            });
        })
    });

    /* Tags */
    let tags = document.querySelectorAll('.tags');
    let garbage = document.querySelector('#garbage');
    /** Creation of tag **/
    document.querySelector('#add_tag').addEventListener(('click'), (e) => {
        let form = new FormData(),
            data =  {
                name: document.querySelector('#tag_name').value,
                color: document.querySelector('#tag_color').value,
            };
        form.append('backoffice_tag', JSON.stringify(data));
        Rails.ajax({
            type: 'POST',
            url: '/backoffice/tags/',
            data: form,
            success: function(response){
                console.log('success: '+response);
            },
            error: function(response){
                console.log('failure: '+response);
            }
        });
    });
    /** Drag events of tags **/
    tags.forEach((tag) => {
        tag.addEventListener(('dragstart'), (e) => {
            e.dataTransfer.effectAllowed = "copyMove";
            e.dataTransfer.setData('tag_id', e.target.id);
            console.log("je suis en train de bouger un tag id "+e.target.id)
        });
        tag.addEventListener('dragover', (e) => {
            e.preventDefault();
            e.dataTransfer.dropEffect = 'copy';
        });
    });


    /** Delete a tag **/
    garbage.addEventListener(('drop'), (e) => {
        Rails.ajax({
            type: 'DELETE',
            url: '/backoffice/tags/'+e.dataTransfer.getData('tag_id'),
            success: function(response){
                console.log('success: '+response);
            },
            error: function(response){
                console.log('failure: '+response);
            }
        });
    });
    /** Subtasks **/
    document.querySelectorAll('.sub_tasks_name').forEach((sub_task_input) => {
        sub_task_input.addEventListener(("change"), (e)=> {
            let sub_task_input = e.target;
            // let parent = e.target.closest('.sub_task_modal');
            // let clone = sub_task.closest('div').cloneNode(true);
            // parent.appendChild(clone);
            if(sub_task_input.id){
                fetch('/backoffice/sub_tasks/'+sub_task_input.id,
                    {
                        method: "PUT",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(
                            { name: sub_task_input.value, status: sub_task_input.previousElementSibling.value, authenticity_token: document.querySelector('#authenticity_token').value},
                        )
                    }
                )
                    .then((res) => res.json())
                    .then((sub_task) => {
                    });
            } else {
                fetch('/backoffice/sub_tasks/',
                    {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(
                            {
                                name: sub_task_input.value,
                                status_id: parseInt(document.querySelector('.sub_task_status').value),
                                authenticity_token: document.querySelector('#authenticity_token').value,
                                task_id: sub_task_input.dataset.task_id
                            },
                        )
                    }
                )
                    .then((res) => res.json())
                    .then((sub_task) => {
                        location.reload();
                        // let template =
                        //     `<div class='list_sub_tasks form-check d-flex align-items-center justify-content-start mb-1'>
                        //           <input type='hidden' class='task_id' value='${sub_task.task_id}'>
                        //           <input class='form-check-input sub_task_checkbox' type='checkbox'>
                        //           <input type='text' placeholder='Sous-tâche' value='${sub_task.name}' id='${sub_task.id}' class='form-control sub_tasks_name border-0 ${sub_task.status_id} ===  4 ? "done" :""' name='sub_task[name]'>
                        //           <i class='fas fa-trash c-pointer remove_subtask'></i>
                        //     </div>`;
                        // document.querySelector('.sub_task_modal').innerHTML += template;
                    })
                    .catch((e) => console.log(e))
            }
        });
    });

    /* Checkboxes */
    document.querySelectorAll('.sub_task_checkbox').forEach((checkbox) => {
        checkbox.addEventListener(('change'), (e) => {
            let checkbox= e.target,
                sub_task = e.target.nextElementSibling;
            if(checkbox.checked){
                sub_task.classList.add('done');
                fetch('/backoffice/sub_tasks/'+sub_task.id,
                    {
                        method: "PUT",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(
                            {
                                name: sub_task.value,
                                status_id: 4,
                                authenticity_token: document.querySelector('#authenticity_token').value,
                                task_id: sub_task.dataset.task_id
                            },
                        )
                    });
            } else {
                sub_task.classList.remove('done');
                fetch('/backoffice/sub_tasks/'+sub_task.id,
                    {
                        method: "PUT",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(
                            {
                                name: sub_task.value,
                                status_id: 3,
                                authenticity_token: document.querySelector('#authenticity_token').value,
                                task_id: sub_task.dataset.task_id
                            },
                        )
                    });
            }
        })
    });

    document.querySelectorAll('.remove_subtask').forEach((trash) => {
        trash.addEventListener(('click'), (e) => {
            let sub_task = e.target.previousElementSibling;
            fetch('/backoffice/sub_tasks/'+sub_task.id,
                {
                    method: "DELETE",
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(
                        {
                            authenticity_token: document.querySelector('#authenticity_token').value,
                        },
                    )
                })
                .then(()=> {
                    sub_task.closest('div').remove();
                })
                .catch((e) => console.log(e));
        });
    });

    document.querySelectorAll(".modal.fade").forEach((modal) => {
        modal.addEventListener("hide.bs.modal",  () => {
            location.reload();
        })
    });

    /* Update a task */
    document.querySelectorAll('.task_name').forEach((task) => {
        task.addEventListener(('change'), (e) => {
            let task_name = e.target.value;
            let task_id= e.target.id;
            let form = new FormData(),
                data =  {
                    name: task_name,
                    authenticity_token: document.querySelector('#authenticity_token').value
                };
            form.append('backoffice_task', JSON.stringify(data));
            fetch('/backoffice/tasks/'+task_id,
                {
                    method: "PUT",
                    body: form
                });
        });
    });

    /* Clone a task */
    document.querySelectorAll('.far.fa-clone').forEach((clone_button) => {
        clone_button.addEventListener(('click'), (e) => {
            let task_id = e.target.closest('.card').id;
            fetch("/backoffice/tasks/"+task_id+"/clone",
                {
                    method: "POST",
                })
                .then(() => location.reload())
                .catch((e) => console.log(e));
        });
    });

    /* Delete a task */
    document.querySelectorAll('.delete_button').forEach((delete_button) => {
        delete_button.addEventListener(('click'), (e) => {
            let task_id = e.target.closest('.card').id;
            fetch("/backoffice/tasks/"+task_id,
                {
                    method: "DELETE",
                })
                .then(() => location.reload())
                .catch((e) => console.log(e));
        });
    });
});



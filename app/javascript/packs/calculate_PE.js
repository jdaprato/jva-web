function calculate_are(are, days, declared_amount){
    return (are*days) - (0.7 * declared_amount * (1-34/100));
}

function calculate_new_days(initial_are, final_are, are_value){
    return (initial_are - final_are) / are_value
}

function initialize_calculation(){
    let are_value = parseFloat(document.querySelector('#are').value),
        days_value = parseInt(document.querySelector('#days').value),
        declared_amount = document.querySelector('#declared_amount');
    fetch('/backoffice/declared_amount')
        .then(res => res.json())
        .then((amount) => {
            declared_amount.value = amount;
            document.querySelector('#final_are').value = calculate_are(are_value, days_value, declared_amount.value).toFixed(2);
            document.querySelector('#new_days').value = calculate_new_days(are_value*days_value, parseFloat(document.querySelector('#final_are').value), are_value).toFixed(0);
            document.querySelector('#total_are').value = (are_value*days_value).toFixed(2);
        })
        .catch((e) => console.log(e))
}

document.addEventListener("turbolinks:load", function() {
    let are = document.querySelector('#are'),
        days = document.querySelector('#days'),
        declared_amount = document.querySelector('#declared_amount');
    initialize_calculation();
    days.addEventListener("change", function(){
        initialize_calculation();
    });

    are.addEventListener("change", function(){
        initialize_calculation();
    });

    declared_amount.addEventListener("change", function() {
        initialize_calculation();
    });

});

json.extract! recurrent_object, :id, :periodicity, :start_date, :end_date, :is_active, :comment, :created_at, :updated_at
json.url recurrent_object_url(recurrent_object, format: :json)

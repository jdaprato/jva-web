json.extract! unit, :id, :name, :description, :status, :login, :password, :is_active, :references, :created_at, :updated_at
json.url unit_url(unit, format: :json)

json.extract! backoffice_achievement, :id, :value, :created_at, :updated_at
json.url backoffice_achievement_url(backoffice_achievement, format: :json)

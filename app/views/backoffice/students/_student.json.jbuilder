json.extract! student, :id, :lastname, :firstname, :email, :dashboard_url, :course_begin_date, :course_end_date, :type, :comment, :is_active, :created_at, :updated_at
json.url student_url(student, format: :json)

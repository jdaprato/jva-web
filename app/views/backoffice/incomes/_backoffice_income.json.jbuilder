json.extract! backoffice_income, :id, :name, :description, :amount, :comment, :invoice_id, :created_at, :updated_at
json.url backoffice_income,_url(backoffice_income, format: :json)

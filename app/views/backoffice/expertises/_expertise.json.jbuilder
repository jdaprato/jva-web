json.extract! expertise, :id, :level, :price, :created_at, :updated_at
json.url expertise_url(expertise, format: :json)

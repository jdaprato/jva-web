json.extract! backoffice_quotation, :id, :number, :validity_date, :amount, :tva, :deposit_percentage, :status, :is_active, :created_at, :updated_at
json.url backoffice_quotation_url(backoffice_quotation, format: :json)

json.extract! backoffice_project, :id, :name, :description, :start_date, :end_date, :type, :status, :is_active, :created_at, :updated_at
json.url backoffice_project_url(backoffice_project, format: :json)

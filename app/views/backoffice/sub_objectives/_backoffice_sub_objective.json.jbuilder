json.extract! backoffice_sub_objective, :id, :name, :description, :type, :value, :status, :created_at, :updated_at
json.url backoffice_sub_objective_url(backoffice_sub_objective, format: :json)

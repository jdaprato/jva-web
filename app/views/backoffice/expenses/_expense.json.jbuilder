json.extract! expense, :id, :name, :description, :status, :amount, :comment, :created_at, :updated_at
json.url expense_url(expense, format: :json)

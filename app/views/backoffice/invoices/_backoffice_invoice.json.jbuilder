json.extract! backoffice_invoice, :id, :number, :payment_deadline, :delivery_date, :TVA, :deposit, :status, :created_at, :updated_at
json.url backoffice_invoice_url(backoffice_invoice, format: :json)

json.extract! oc_session, :id, :session_date, :session_type, :status, :student_id, :created_at, :updated_at
json.url oc_session_url(oc_session, format: :json)

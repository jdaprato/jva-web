json.extract! backoffice_contact, :id, :lastname, :firstname, :email, :phone, :is_active, :created_at, :updated_at
json.url backoffice_contact_url(backoffice_contact, format: :json)

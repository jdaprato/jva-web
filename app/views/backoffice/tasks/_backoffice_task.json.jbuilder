json.extract! backoffice_task, :id, :name, :priority, :status, :created_at, :updated_at
json.url backoffice_task_url(backoffice_task, format: :json)

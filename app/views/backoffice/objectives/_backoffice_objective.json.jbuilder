json.extract! backoffice_objective, :id, :name, :description, :start_date, :end_date, :status, :created_at, :updated_at
json.url backoffice_objective_url(backoffice_objective, format: :json)

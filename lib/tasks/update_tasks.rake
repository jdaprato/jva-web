#to launch every day at 00:00
namespace :update_tasks do
  desc 'Put undone tasks from previous weeks into todo list'
  task due_date: :environment do
    @tasks = Task.where(status_id: 3)
    @tasks.each do |task|
      if !task.due_date.nil? && task.due_date < Date.today
        task.update!({id: task.id, due_date: nil})
      end
    end
  end
end

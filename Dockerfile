FROM ruby:3.0.2-slim

RUN set -eux; \
    apt update && \
    apt install -y nodejs git build-essential libpq-dev postgresql-client npm webpack && \
    npm install --global yarn && \
    yarn add @rails/webpacker && \
    bundle config --global frozen 1
ADD . /jva-web
WORKDIR /jva-web
COPY Gemfile Gemfile.lock ./

RUN bundle install

EXPOSE 3000
CMD ./start.sh

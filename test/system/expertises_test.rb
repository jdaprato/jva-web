require "application_system_test_case"

class ExpertisesTest < ApplicationSystemTestCase
  setup do
    @expertise = expertises(:one)
  end

  test "visiting the index" do
    visit expertises_url
    assert_selector "h1", text: "Expertises"
  end

  test "creating a Expertise" do
    visit expertises_url
    click_on "New Expertise"

    fill_in "Level", with: @expertise.name
    fill_in "Price", with: @expertise.price
    click_on "Create Expertise"

    assert_text "Expertise was successfully created"
    click_on "Back"
  end

  test "updating a Expertise" do
    visit expertises_url
    click_on "Edit", match: :first

    fill_in "Level", with: @expertise.name
    fill_in "Price", with: @expertise.price
    click_on "Update Expertise"

    assert_text "Expertise was successfully updated"
    click_on "Back"
  end

  test "destroying a Expertise" do
    visit expertises_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Expertise was successfully destroyed"
  end
end

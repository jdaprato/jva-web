require "application_system_test_case"

class OcSessionsTest < ApplicationSystemTestCase
  setup do
    @oc_session = oc_sessions(:one)
  end

  test "visiting the index" do
    visit oc_sessions_url
    assert_selector "h1", text: "Oc Sessions"
  end

  test "creating a Oc session" do
    visit oc_sessions_url
    click_on "New Oc Session"

    fill_in "Session date", with: @oc_session.session_date
    fill_in "Session type", with: @oc_session.session_type
    fill_in "Status", with: @oc_session.status
    fill_in "Student", with: @oc_session.student_id
    click_on "Create Oc session"

    assert_text "Oc session was successfully created"
    click_on "Back"
  end

  test "updating a Oc session" do
    visit oc_sessions_url
    click_on "Edit", match: :first

    fill_in "Session date", with: @oc_session.session_date
    fill_in "Session type", with: @oc_session.session_type
    fill_in "Status", with: @oc_session.status
    fill_in "Student", with: @oc_session.student_id
    click_on "Update Oc session"

    assert_text "Oc session was successfully updated"
    click_on "Back"
  end

  test "destroying a Oc session" do
    visit oc_sessions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Oc session was successfully destroyed"
  end
end

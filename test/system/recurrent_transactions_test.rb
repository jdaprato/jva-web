require "application_system_test_case"

class RecurrentTransactionsTest < ApplicationSystemTestCase
  setup do
    @recurrent_object = recurrent_objects(:one)
  end

  test "visiting the index" do
    visit recurrent_objects_url
    assert_selector "h1", text: "Recurrent Transactions"
  end

  test "creating a Recurrent transaction" do
    visit recurrent_objects_url
    click_on "New Recurrent Transaction"

    fill_in "Comment", with: @recurrent_object.comment
    fill_in "End date", with: @recurrent_object.end_date
    check "Is active" if @recurrent_object.is_active
    fill_in "Periodicity", with: @recurrent_object.periodicity
    fill_in "Start date", with: @recurrent_object.start_date
    click_on "Create Recurrent transaction"

    assert_text "Recurrent transaction was successfully created"
    click_on "Back"
  end

  test "updating a Recurrent transaction" do
    visit recurrent_objects_url
    click_on "Edit", match: :first

    fill_in "Comment", with: @recurrent_object.comment
    fill_in "End date", with: @recurrent_object.end_date
    check "Is active" if @recurrent_object.is_active
    fill_in "Periodicity", with: @recurrent_object.periodicity
    fill_in "Start date", with: @recurrent_object.start_date
    click_on "Update Recurrent transaction"

    assert_text "Recurrent transaction was successfully updated"
    click_on "Back"
  end

  test "destroying a Recurrent transaction" do
    visit recurrent_objects_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Recurrent transaction was successfully destroyed"
  end
end

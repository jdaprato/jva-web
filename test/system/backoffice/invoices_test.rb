require "application_system_test_case"

class Backoffice::InvoicesTest < ApplicationSystemTestCase
  setup do
    @backoffice_invoice = backoffice_invoices(:one)
  end

  test "visiting the index" do
    visit backoffice_invoices_url
    assert_selector "h1", text: "Backoffice/Invoices"
  end

  test "creating a Invoice" do
    visit backoffice_invoices_url
    click_on "New Backoffice/Invoice"

    check "Tva" if @backoffice_invoice.TVA
    fill_in "Delivery date", with: @backoffice_invoice.delivery_date
    check "Deposit" if @backoffice_invoice.deposit
    fill_in "Number", with: @backoffice_invoice.number
    fill_in "Payment deadline", with: @backoffice_invoice.payment_deadline
    fill_in "Status", with: @backoffice_invoice.status
    click_on "Create Invoice"

    assert_text "Invoice was successfully created"
    click_on "Back"
  end

  test "updating a Invoice" do
    visit backoffice_invoices_url
    click_on "Edit", match: :first

    check "Tva" if @backoffice_invoice.TVA
    fill_in "Delivery date", with: @backoffice_invoice.delivery_date
    check "Deposit" if @backoffice_invoice.deposit
    fill_in "Number", with: @backoffice_invoice.number
    fill_in "Payment deadline", with: @backoffice_invoice.payment_deadline
    fill_in "Status", with: @backoffice_invoice.status
    click_on "Update Invoice"

    assert_text "Invoice was successfully updated"
    click_on "Back"
  end

  test "destroying a Invoice" do
    visit backoffice_invoices_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Invoice was successfully destroyed"
  end
end

require "application_system_test_case"

class Backoffice::SubObjectivesTest < ApplicationSystemTestCase
  setup do
    @backoffice_sub_objective = backoffice_sub_objectives(:one)
  end

  test "visiting the index" do
    visit backoffice_sub_objectives_url
    assert_selector "h1", text: "Backoffice/Sub Objectives"
  end

  test "creating a Sub objective" do
    visit backoffice_sub_objectives_url
    click_on "New Backoffice/Sub Objective"

    fill_in "Description", with: @backoffice_sub_objective.description
    fill_in "Name", with: @backoffice_sub_objective.name
    fill_in "Status", with: @backoffice_sub_objective.status
    fill_in "Type", with: @backoffice_sub_objective.type
    fill_in "Value", with: @backoffice_sub_objective.value
    click_on "Create Sub objective"

    assert_text "Sub objective was successfully created"
    click_on "Back"
  end

  test "updating a Sub objective" do
    visit backoffice_sub_objectives_url
    click_on "Edit", match: :first

    fill_in "Description", with: @backoffice_sub_objective.description
    fill_in "Name", with: @backoffice_sub_objective.name
    fill_in "Status", with: @backoffice_sub_objective.status
    fill_in "Type", with: @backoffice_sub_objective.type
    fill_in "Value", with: @backoffice_sub_objective.value
    click_on "Update Sub objective"

    assert_text "Sub objective was successfully updated"
    click_on "Back"
  end

  test "destroying a Sub objective" do
    visit backoffice_sub_objectives_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sub objective was successfully destroyed"
  end
end

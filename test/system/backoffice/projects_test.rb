require "application_system_test_case"

class Backoffice::ProjectsTest < ApplicationSystemTestCase
  setup do
    @project = backoffice_projects(:one)
  end

  test "visiting the index" do
    visit backoffice_projects_url
    assert_selector "h1", text: "Backoffice/Projects"
  end

  test "creating a Project" do
    visit backoffice_projects_url
    click_on "New Backoffice/Project"

    fill_in "Description", with: @project.description
    fill_in "End date", with: @project.end_date
    check "Is active" if @project.is_active
    fill_in "Name", with: @project.name
    fill_in "Start date", with: @project.start_date
    fill_in "Status", with: @project.status
    fill_in "Type", with: @project.type
    click_on "Create Project"

    assert_text "Project was successfully created"
    click_on "Back"
  end

  test "updating a Project" do
    visit backoffice_projects_url
    click_on "Edit", match: :first

    fill_in "Description", with: @project.description
    fill_in "End date", with: @project.end_date
    check "Is active" if @project.is_active
    fill_in "Name", with: @project.name
    fill_in "Start date", with: @project.start_date
    fill_in "Status", with: @project.status
    fill_in "Type", with: @project.type
    click_on "Update Project"

    assert_text "Project was successfully updated"
    click_on "Back"
  end

  test "destroying a Project" do
    visit backoffice_projects_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Project was successfully destroyed"
  end
end

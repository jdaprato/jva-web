require "application_system_test_case"

class Backoffice::ContactsTest < ApplicationSystemTestCase
  setup do
    @backoffice_contact = backoffice_contacts(:one)
  end

  test "visiting the index" do
    visit backoffice_contacts_url
    assert_selector "h1", text: "Backoffice/Contacts"
  end

  test "creating a Contact" do
    visit backoffice_contacts_url
    click_on "New Backoffice/Contact"

    fill_in "Email", with: @backoffice_contact.email
    fill_in "Firstname", with: @backoffice_contact.firstname
    check "Is active" if @backoffice_contact.is_active
    fill_in "Lastname", with: @backoffice_contact.lastname
    fill_in "Phone", with: @backoffice_contact.phone
    click_on "Create Contact"

    assert_text "Contact was successfully created"
    click_on "Back"
  end

  test "updating a Contact" do
    visit backoffice_contacts_url
    click_on "Edit", match: :first

    fill_in "Email", with: @backoffice_contact.email
    fill_in "Firstname", with: @backoffice_contact.firstname
    check "Is active" if @backoffice_contact.is_active
    fill_in "Lastname", with: @backoffice_contact.lastname
    fill_in "Phone", with: @backoffice_contact.phone
    click_on "Update Contact"

    assert_text "Contact was successfully updated"
    click_on "Back"
  end

  test "destroying a Contact" do
    visit backoffice_contacts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Contact was successfully destroyed"
  end
end

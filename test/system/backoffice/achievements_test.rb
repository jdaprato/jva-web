require "application_system_test_case"

class Backoffice::AchievementsTest < ApplicationSystemTestCase
  setup do
    @backoffice_achievement = backoffice_achievements(:one)
  end

  test "visiting the index" do
    visit backoffice_achievements_url
    assert_selector "h1", text: "Backoffice/Achievements"
  end

  test "creating a Achievement" do
    visit backoffice_achievements_url
    click_on "New Backoffice/Achievement"

    fill_in "Value", with: @backoffice_achievement.value
    click_on "Create Achievement"

    assert_text "Achievement was successfully created"
    click_on "Back"
  end

  test "updating a Achievement" do
    visit backoffice_achievements_url
    click_on "Edit", match: :first

    fill_in "Value", with: @backoffice_achievement.value
    click_on "Update Achievement"

    assert_text "Achievement was successfully updated"
    click_on "Back"
  end

  test "destroying a Achievement" do
    visit backoffice_achievements_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Achievement was successfully destroyed"
  end
end

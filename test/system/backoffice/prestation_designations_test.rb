require "application_system_test_case"

class Backoffice::presDesignationsTest < ApplicationSystemTestCase
  setup do
    @backoffice_pres_designation = backoffice_pres_designations(:one)
  end

  test "visiting the index" do
    visit backoffice_pres_designations_url
    assert_selector "h1", text: "Backoffice/pres Designations"
  end

  test "creating a pres designation" do
    visit backoffice_pres_designations_url
    click_on "New Backoffice/pres Designation"

    fill_in "Designation", with: @backoffice_pres_designation.designation
    fill_in "Number", with: @backoffice_pres_designation.number
    fill_in "Quantity", with: @backoffice_pres_designation.quantity
    fill_in "Unit price", with: @backoffice_pres_designation.unit_price
    click_on "Create pres designation"

    assert_text "pres designation was successfully created"
    click_on "Back"
  end

  test "updating a pres designation" do
    visit backoffice_pres_designations_url
    click_on "Edit", match: :first

    fill_in "Designation", with: @backoffice_pres_designation.designation
    fill_in "Number", with: @backoffice_pres_designation.number
    fill_in "Quantity", with: @backoffice_pres_designation.quantity
    fill_in "Unit price", with: @backoffice_pres_designation.unit_price
    click_on "Update pres designation"

    assert_text "pres designation was successfully updated"
    click_on "Back"
  end

  test "destroying a pres designation" do
    visit backoffice_pres_designations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "pres designation was successfully destroyed"
  end
end

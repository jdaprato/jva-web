require "application_system_test_case"

class Backoffice::QuotationsTest < ApplicationSystemTestCase
  setup do
    @backoffice_quotation = backoffice_quotations(:one)
  end

  test "visiting the index" do
    visit backoffice_quotations_url
    assert_selector "h1", text: "Backoffice/Quotations"
  end

  test "creating a Quotation" do
    visit backoffice_quotations_url
    click_on "New Backoffice/Quotation"

    fill_in "Amount", with: @backoffice_quotation.amount
    fill_in "Deposit percentage", with: @backoffice_quotation.deposit_percentage
    check "Is active" if @backoffice_quotation.is_active
    fill_in "Number", with: @backoffice_quotation.number
    fill_in "Status", with: @backoffice_quotation.status
    check "Tva" if @backoffice_quotation.tva
    fill_in "Validity date", with: @backoffice_quotation.validity_date
    click_on "Create Quotation"

    assert_text "Quotation was successfully created"
    click_on "Back"
  end

  test "updating a Quotation" do
    visit backoffice_quotations_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @backoffice_quotation.amount
    fill_in "Deposit percentage", with: @backoffice_quotation.deposit_percentage
    check "Is active" if @backoffice_quotation.is_active
    fill_in "Number", with: @backoffice_quotation.number
    fill_in "Status", with: @backoffice_quotation.status
    check "Tva" if @backoffice_quotation.tva
    fill_in "Validity date", with: @backoffice_quotation.validity_date
    click_on "Update Quotation"

    assert_text "Quotation was successfully updated"
    click_on "Back"
  end

  test "destroying a Quotation" do
    visit backoffice_quotations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Quotation was successfully destroyed"
  end
end

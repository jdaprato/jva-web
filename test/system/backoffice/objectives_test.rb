require "application_system_test_case"

class Backoffice::ObjectivesTest < ApplicationSystemTestCase
  setup do
    @backoffice_objective = backoffice_objectives(:one)
  end

  test "visiting the index" do
    visit backoffice_objectives_url
    assert_selector "h1", text: "Backoffice/Objectives"
  end

  test "creating a Objective" do
    visit backoffice_objectives_url
    click_on "New Backoffice/Objective"

    fill_in "Description", with: @backoffice_objective.description
    fill_in "End date", with: @backoffice_objective.end_date
    fill_in "Name", with: @backoffice_objective.name
    fill_in "Start date", with: @backoffice_objective.start_date
    fill_in "Status", with: @backoffice_objective.status
    click_on "Create Objective"

    assert_text "Objective was successfully created"
    click_on "Back"
  end

  test "updating a Objective" do
    visit backoffice_objectives_url
    click_on "Edit", match: :first

    fill_in "Description", with: @backoffice_objective.description
    fill_in "End date", with: @backoffice_objective.end_date
    fill_in "Name", with: @backoffice_objective.name
    fill_in "Start date", with: @backoffice_objective.start_date
    fill_in "Status", with: @backoffice_objective.status
    click_on "Update Objective"

    assert_text "Objective was successfully updated"
    click_on "Back"
  end

  test "destroying a Objective" do
    visit backoffice_objectives_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Objective was successfully destroyed"
  end
end

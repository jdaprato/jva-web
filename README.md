# README

JVA-WEB is a ruby project with two major purposes: present the activities of the company with a **frontoffice** accessible to all, and offer a **backoffice** with multiple functionnalities to manage the activities of the company such as editing quotations, bills, creating projects, customers, companies etc ...

**JVA-WEB technical requirements:**

* **Ruby version : 3.0.1**
* **Rails version : 6.1.4**
* **Bundle version : 2.2.11**


**Database creation:**

JVA-WEB is built to run with a **PostGreSQL Database.**
There is no need to create a database (it will be done automatically with a command line), but you need to create at least a user with all privileges with the username given into the **database.yml** file (present in the root of the config folder) or change the username according to your configuration.
The password has to be stored directly in your production environment with the following command:
`export JVA_WEB_DATABASE_PASSWORD='your password here'`

When this is done, you can use the following command line to create the db:
`rake db:create`

Finally run the migrations with the following command:
`rails db:migrate`


**How to run the test :**

This application uses MiniTest.
Tests are run under `RAILS_ENV=test` with the command `rails test`

**Deployment instructions (without docker):**

First you need to install ruby on your system, version 3.0.1.

Then you need to install Rails:
`gem install rails`

Then in the application folder run `bundle install`.

Finally load the server with the `rails s` command.

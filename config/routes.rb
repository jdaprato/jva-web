Rails.application.routes.draw do

  devise_for :users, :controllers => {:sessions => "sessions"}

  root :to => "home#index"
  resources :contacts, only: [:new, :create]
  match 'contact' => 'contact#create', :as => 'contact', :via => :post

  namespace :backoffice do
    resources :oc_sessions
    resources :expertises
    resources :courses
    resources :students
    resources :sub_tasks
    root 'dashboard#index'
    resources :invoices
    resources :incomes
    resources :expenses
    resources :recurrent_objects
    resources :presta_designations
    resources :quotations
    resources :projects
    resources :contacts
    resources :achievements
    resources :sub_objectives
    resources :objectives
    resources :tasks do
      post '/clone', :to => 'tasks#clone_task'
    end
    resources :tags
    get 'kanban' => 'tasks#kanban'
    resources :companies
    resources :quotation_pdfs, only: [:show]
    resources :invoice_pdfs, only: [:show]
    resources :units
    devise_for :users,  controllers: {
      :registrations => "backoffice/registrations",
    }
    get '/callback', to: 'dashboard#callback', as: 'callback'

    get '/declared_amount', to: 'dashboard#get_declared_amount', as: 'declared_amount'
  end

end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Status.create!(
  [
    { name: 'is_draft' },
    { name: 'is_paused' },
    { name: 'is_ongoing' },
    { name: 'is_finished' },
    { name: 'is_archived' },
  ]
)

Priority.create!(
  [
    {name: 'low'},
    {name: 'medium'},
    {name: 'high'},
  ]
)
# Demo account
10.times do
  Company.create!(
    user_id: 1,
    name: Faker::Company.name,
    description: Faker::Company.catch_phrase
  )

Contact.create!(
  [
    {
      lastname: "Dupond",
      firstname: "Jean",
      email: "jean.dupond@test.fr",
      company_id: 1
    },
    {
      lastname: "Dapos",
      firstname: "Elisa",
      email: "el.dapos@gmail.fr",
      company_id: 2
    },
    {
      lastname: "Carol",
      firstname: "Ethan",
      email: "ethanc@gmail.fr",
      company_id: 3
    },
  ]
)
Income.create!(
[
  {
    name: "Salaire prestation",
    amount: 150,
    credit_date: "10/02/2021",
    user_id: 1
  },
  {
    name: "Facture OC",
    amount: 3600,
    credit_date: "08/04/2021",
    user_id: 1
  },
  {
    name: "Site internet",
    amount: 5000,
    credit_date: "01/01/2021",
    user_id: 1
  },
]
)


end



class AddIncomeTypeToIncomes < ActiveRecord::Migration[6.1]
  def change
    add_column :incomes, :income_type, :string, :null => false, :default => "ca"
  end
end

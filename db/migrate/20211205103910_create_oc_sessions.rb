class CreateOcSessions < ActiveRecord::Migration[6.1]
  def change
    create_table :oc_sessions do |t|
      t.datetime :session_date
      t.string :session_type
      t.string :status
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end

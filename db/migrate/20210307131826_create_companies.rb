class CreateCompanies < ActiveRecord::Migration[6.1]
  def change
    create_table :companies do |t|
      t.string :name, :null => false
      t.text :description
      t.string :complement_address
      t.string :street_address
      t.string :zipcode_address
      t.string :city_address
      t.string :email
      t.string :phone
      t.boolean :is_active, :null => false, :default => true

      t.timestamps
    end
  end
end

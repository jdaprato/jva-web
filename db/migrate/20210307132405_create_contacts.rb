class CreateContacts < ActiveRecord::Migration[6.1]
  def change
    create_table :contacts do |t|
      t.string :lastname, :null => false
      t.string :firstname, :null => false
      t.string :email
      t.string :phone
      t.boolean :is_active, :null => false, :default => true
      t.belongs_to :company, foreign_key: true, :null => true
      t.index [:firstname, :lastname], unique: true

      t.timestamps
    end
  end
end

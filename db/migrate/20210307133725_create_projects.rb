class CreateProjects < ActiveRecord::Migration[6.1]
  def change
    create_table :projects do |t|
      t.string :name, :null => false
      t.text :description
      t.date :start_date, :null => false
      t.date :end_date
      t.string :status, :null => false
      t.boolean :is_active, :null => false, :default => true
      t.belongs_to :company, foreign_key: true, :null => true

      t.timestamps
    end
  end
end

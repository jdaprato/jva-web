class ChangeNullValueToAchievements < ActiveRecord::Migration[6.1]
  def change
    change_column_null :achievements, :sub_objective_id, true
  end
end

class CreateStudents < ActiveRecord::Migration[6.1]
  def change
    create_table :students do |t|
      t.string :lastname
      t.string :firstname
      t.string :email
      t.text :dashboard_url
      t.date :course_begin_date
      t.date :course_end_date
      t.string :course_type
      t.text :comment
      t.boolean :is_active, default: true

      t.belongs_to :course,  foreign_key: true
      t.timestamps
    end
  end
end

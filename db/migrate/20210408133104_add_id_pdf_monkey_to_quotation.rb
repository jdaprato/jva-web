class AddIdPdfMonkeyToQuotation < ActiveRecord::Migration[6.1]
  def up
    add_column :quotations, :id_pdfmonkey, :string
  end

  def down
    remove_column :quotations, :id_pdfmonkey, :string
  end
end

class CreateUnits < ActiveRecord::Migration[6.1]
  def change
    create_table :units do |t|
      t.string :name
      t.text :description
      t.string :status
      t.string :login
      t.string :password
      t.boolean :is_active
      t.belongs_to :user, foreign_key: true, :null => true

      t.timestamps
    end
  end
end

class AddIdPdfMonkeyToInvoices < ActiveRecord::Migration[6.1]
  def up
    add_column :invoices, :id_pdfmonkey, :string
  end

  def down
    remove_column :invoices, :id_pdfmonkey, :string
  end
end

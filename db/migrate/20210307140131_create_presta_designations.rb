class CreatePrestaDesignations < ActiveRecord::Migration[6.1]
  def change
    create_table :presta_designations do |t|
      t.string :digit, :null => false
      t.text :designation, :null => false
      t.integer :quantity, :null => false
      t.float :unit_price, :null => false
      t.belongs_to :quotation, foreign_key: true
      t.belongs_to :invoice,  foreign_key: true

      t.timestamps
    end
  end
end

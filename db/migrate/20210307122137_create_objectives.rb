class CreateObjectives < ActiveRecord::Migration[6.1]
  def change
    create_table :objectives do |t|
      t.string :name, :null => false
      t.text :description
      t.date :start_date, :null => false
      t.date :end_date
      t.string :status, :null => false, :default => "is_draft"

      t.timestamps
    end
  end
end

class CreateInvoices < ActiveRecord::Migration[6.1]
  def change
    create_table :invoices do |t|
      t.string :name, :null => false
      t.string :digit, :null => false, :unique => true
      t.date :payment_deadline, :null => false
      t.date :delivery_date, :null => false
      t.boolean :TVA, :null => false, :default => false
      t.boolean :deposit, :null => false, :default => false
      t.string :status, :null => false
      t.belongs_to :quotation, foreign_key: true
      t.belongs_to :company, foreign_key: true

      t.timestamps
    end
  end
end

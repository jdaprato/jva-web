class AddStatusToSubObjective < ActiveRecord::Migration[6.1]
  def change
    add_reference :sub_objectives, :status, null: true, foreign_key: true
    remove_column :sub_objectives, :status
  end
end

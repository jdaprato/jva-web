class CreateIncomes < ActiveRecord::Migration[6.1]
  def change
    create_table :incomes do |t|
      t.string :name
      t.float :amount
      t.text :description
      t.text :comment
      t.date :credit_date

      t.references :invoice

      t.timestamps
    end
  end
end

class CreateSubObjectives < ActiveRecord::Migration[6.1]
  def change
    create_table :sub_objectives do |t|
      t.string :name, :null => false
      t.text :description
      t.string :sub_objective_category, :null => false
      t.string :value, :null => false
      t.string :status, :null => false, :default => "is_ongoing"
      t.belongs_to :objective, foreign_key: true, :null => false
      t.timestamps
    end
  end
end

class CreateRecurrentObjects< ActiveRecord::Migration[6.1]
  def change
    create_table :recurrent_objects do |t|
      t.string :periodicity, null: false
      t.date :start_date, null: false
      t.date :end_date, null: true
      t.boolean :is_active, null: false, default: true
      t.text :comment, null: true
      t.belongs_to :expense,  foreign_key: true
      t.belongs_to :income,  foreign_key: true
      t.belongs_to :sub_objective,  foreign_key: true
      t.timestamps
    end
  end
end

class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :name, null: false
      t.string :task_category, null: false
      t.references :status, null: false
      t.references :priority, null: false
      t.timestamps
    end
  end
end

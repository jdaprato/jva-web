class AddExpenseTypeToExpense < ActiveRecord::Migration[6.1]
  def change
    add_column :expenses, :expense_type, :string, :null => false, :default => "expense"
  end
end

class CreateExpenses < ActiveRecord::Migration[6.1]
  def change
    create_table :expenses do |t|
      t.string :name
      t.text :description
      t.float :amount
      t.date :debit_date
      t.text :comment
      t.integer :parent_expense
      t.references :user, null: false

      t.timestamps
    end
  end
end

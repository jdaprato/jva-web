class CreateQuotations < ActiveRecord::Migration[6.1]
  def change
    create_table :quotations do |t|
      t.string :name, :null => false
      t.string :digit, :null => false
      t.date :validity_date, :null => false
      t.float :total_ttc, :null => false
      t.boolean :tva, :null => false
      t.integer :deposit_percentage, :null => false
      t.string :status, :null => false
      t.boolean :is_active, :null => false, :default => true
      t.belongs_to :company,  foreign_key: true
      t.belongs_to :project, foreign_key: true

      t.timestamps
    end
  end
end

class AddOcSessionsCountToStudents < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :oc_sessions_count, :integer
  end
end

class CreateAchievements < ActiveRecord::Migration[6.1]
  def change
    create_table :achievements do |t|
      t.string :value
      t.belongs_to :sub_objective, foreign_key: true, :null => false

      t.timestamps
    end
  end
end

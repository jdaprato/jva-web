class AddExpertiseToProjects < ActiveRecord::Migration[6.1]
  def change
    add_reference :projects, :expertise, null: true, foreign_key: true
  end
end

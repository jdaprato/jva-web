class AddProgressionToSubObjective < ActiveRecord::Migration[6.1]
  def change
    add_column :sub_objectives, :progression, :integer, default: 0
  end
end

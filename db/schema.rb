# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_28_104123) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "achievements", force: :cascade do |t|
    t.string "value"
    t.bigint "sub_objective_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["sub_objective_id"], name: "index_achievements_on_sub_objective_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.string "complement_address"
    t.string "street_address"
    t.string "zipcode_address"
    t.string "city_address"
    t.string "email"
    t.string "phone"
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.index ["user_id"], name: "index_companies_on_user_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "lastname", null: false
    t.string "firstname", null: false
    t.string "email"
    t.string "phone"
    t.boolean "is_active", default: true, null: false
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.index ["company_id"], name: "index_contacts_on_company_id"
    t.index ["firstname", "lastname"], name: "index_contacts_on_firstname_and_lastname", unique: true
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "slug"
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "expenses", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.float "amount"
    t.date "debit_date"
    t.text "comment"
    t.integer "parent_expense"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "expense_type", default: "expense", null: false
    t.index ["user_id"], name: "index_expenses_on_user_id"
  end

  create_table "expertises", force: :cascade do |t|
    t.string "name"
    t.integer "price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "incomes", force: :cascade do |t|
    t.string "name"
    t.float "amount"
    t.text "description"
    t.text "comment"
    t.date "credit_date"
    t.bigint "invoice_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.string "income_type", default: "ca", null: false
    t.index ["invoice_id"], name: "index_incomes_on_invoice_id"
    t.index ["user_id"], name: "index_incomes_on_user_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.string "name", null: false
    t.string "digit", null: false
    t.date "payment_deadline", null: false
    t.date "delivery_date", null: false
    t.boolean "TVA", default: false, null: false
    t.boolean "deposit", default: false, null: false
    t.string "status", null: false
    t.bigint "quotation_id"
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "id_pdfmonkey"
    t.bigint "user_id", null: false
    t.index ["company_id"], name: "index_invoices_on_company_id"
    t.index ["quotation_id"], name: "index_invoices_on_quotation_id"
    t.index ["user_id"], name: "index_invoices_on_user_id"
  end

  create_table "objectives", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.date "start_date", null: false
    t.date "end_date"
    t.string "status", default: "is_draft", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.index ["user_id"], name: "index_objectives_on_user_id"
  end

  create_table "oc_sessions", force: :cascade do |t|
    t.datetime "session_date"
    t.string "session_type"
    t.string "status"
    t.bigint "student_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "project_id"
    t.bigint "course_id"
    t.index ["course_id"], name: "index_oc_sessions_on_course_id"
    t.index ["project_id"], name: "index_oc_sessions_on_project_id"
    t.index ["student_id"], name: "index_oc_sessions_on_student_id"
  end

  create_table "presta_designations", force: :cascade do |t|
    t.string "digit", null: false
    t.text "designation", null: false
    t.integer "quantity", null: false
    t.float "unit_price", null: false
    t.bigint "quotation_id"
    t.bigint "invoice_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["invoice_id"], name: "index_presta_designations_on_invoice_id"
    t.index ["quotation_id"], name: "index_presta_designations_on_quotation_id"
  end

  create_table "priorities", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.date "start_date"
    t.date "end_date"
    t.string "status", null: false
    t.boolean "is_active", default: true, null: false
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.string "slug"
    t.bigint "course_id"
    t.bigint "expertise_id"
    t.index ["company_id"], name: "index_projects_on_company_id"
    t.index ["course_id"], name: "index_projects_on_course_id"
    t.index ["expertise_id"], name: "index_projects_on_expertise_id"
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "quotations", force: :cascade do |t|
    t.string "name", null: false
    t.string "digit", null: false
    t.date "validity_date", null: false
    t.float "total_ttc", null: false
    t.boolean "tva", null: false
    t.integer "deposit_percentage", null: false
    t.string "status", null: false
    t.boolean "is_active", default: true, null: false
    t.bigint "company_id"
    t.bigint "project_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "id_pdfmonkey"
    t.bigint "user_id", null: false
    t.index ["company_id"], name: "index_quotations_on_company_id"
    t.index ["project_id"], name: "index_quotations_on_project_id"
    t.index ["user_id"], name: "index_quotations_on_user_id"
  end

  create_table "recurrent_objects", force: :cascade do |t|
    t.string "periodicity", null: false
    t.date "start_date", null: false
    t.date "end_date"
    t.boolean "is_active", default: true, null: false
    t.text "comment"
    t.bigint "expense_id"
    t.bigint "income_id"
    t.bigint "sub_objective_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["expense_id"], name: "index_recurrent_objects_on_expense_id"
    t.index ["income_id"], name: "index_recurrent_objects_on_income_id"
    t.index ["sub_objective_id"], name: "index_recurrent_objects_on_sub_objective_id"
  end

  create_table "statuses", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "students", force: :cascade do |t|
    t.string "lastname"
    t.string "firstname"
    t.string "email"
    t.text "dashboard_url"
    t.date "course_begin_date"
    t.date "course_end_date"
    t.string "course_type"
    t.text "comment"
    t.boolean "is_active"
    t.bigint "course_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "project_id"
    t.integer "oc_sessions_count"
    t.index ["course_id"], name: "index_students_on_course_id"
    t.index ["project_id"], name: "index_students_on_project_id"
  end

  create_table "sub_objectives", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.string "sub_objective_category", null: false
    t.string "value", null: false
    t.bigint "objective_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "progression", default: 0
    t.bigint "status_id"
    t.index ["objective_id"], name: "index_sub_objectives_on_objective_id"
    t.index ["status_id"], name: "index_sub_objectives_on_status_id"
  end

  create_table "sub_tasks", force: :cascade do |t|
    t.string "name"
    t.bigint "status_id", null: false
    t.bigint "task_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["status_id"], name: "index_sub_tasks_on_status_id"
    t.index ["task_id"], name: "index_sub_tasks_on_task_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.string "color"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.string "name", null: false
    t.string "task_category", null: false
    t.bigint "status_id", null: false
    t.bigint "priority_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.date "due_date"
    t.bigint "tag_id"
    t.bigint "sub_objective_id"
    t.index ["priority_id"], name: "index_tasks_on_priority_id"
    t.index ["status_id"], name: "index_tasks_on_status_id"
    t.index ["sub_objective_id"], name: "index_tasks_on_sub_objective_id"
    t.index ["tag_id"], name: "index_tasks_on_tag_id"
    t.index ["user_id"], name: "index_tasks_on_user_id"
  end

  create_table "units", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "status"
    t.string "login"
    t.string "password"
    t.boolean "is_active"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_units_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "achievements", "sub_objectives"
  add_foreign_key "companies", "users"
  add_foreign_key "contacts", "companies"
  add_foreign_key "contacts", "users"
  add_foreign_key "incomes", "users"
  add_foreign_key "invoices", "companies"
  add_foreign_key "invoices", "quotations"
  add_foreign_key "invoices", "users"
  add_foreign_key "objectives", "users"
  add_foreign_key "oc_sessions", "courses"
  add_foreign_key "oc_sessions", "projects"
  add_foreign_key "oc_sessions", "students"
  add_foreign_key "presta_designations", "invoices"
  add_foreign_key "presta_designations", "quotations"
  add_foreign_key "projects", "companies"
  add_foreign_key "projects", "courses"
  add_foreign_key "projects", "expertises"
  add_foreign_key "projects", "users"
  add_foreign_key "quotations", "companies"
  add_foreign_key "quotations", "projects"
  add_foreign_key "quotations", "users"
  add_foreign_key "recurrent_objects", "expenses"
  add_foreign_key "recurrent_objects", "incomes"
  add_foreign_key "recurrent_objects", "sub_objectives"
  add_foreign_key "students", "courses"
  add_foreign_key "students", "projects"
  add_foreign_key "sub_objectives", "objectives"
  add_foreign_key "sub_objectives", "statuses"
  add_foreign_key "sub_tasks", "statuses"
  add_foreign_key "sub_tasks", "tasks"
  add_foreign_key "tasks", "sub_objectives"
  add_foreign_key "tasks", "tags"
  add_foreign_key "tasks", "users"
  add_foreign_key "units", "users"
end

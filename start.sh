#!/bin/bash
set -eux
rake assets:precompile
bin/webpack
rake db:create
rake db:migrate || rake db:setup
rails s -b '0.0.0.0'